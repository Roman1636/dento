        <div id="shadowBox"></div>
        <!-- popupBoxContainer -->
        <div id="popupBoxContainer">
            <div id="popupBox">
                <div id="popupClose">
                    <div id="popupCloseBtn"></div>
                </div>
                <div id="popupHead"></div>
                <div id="popupText">
                    <img onload="popupShow()" src="css/images/line10right_img1_fullsize.jpg">
                </div>
            </div>
        </div>
        <!-- END:popupBoxContainer -->
        <!-- mailBoxContainer -->
        <div id="mailBoxContainer">
            <div id="mailBox">
                <div id="mailClose">
                    <div id="mailCloseBtn"></div>
                </div>
                <input type="text" class="formInput clientName" placeholder="Ваше имя">
                <input type="text" class="formInput clientPhone" placeholder="Ваш телефон">
                <div class="formBtn">Отправить заявку</div>
            </div>
        </div>
        <!-- END:mailBoxContainer -->


        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/landing-css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/landing-css/select2.min.css" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/landing-css/main.min.css" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/landing-css/form.min.css" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/landing-css/landing-fixes.css" />
		
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/landing-js/jquery-3.1.0.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/landing-js/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/landing-js/select2.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/landing-js/bootstrap.min.js" async></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/landing-js/contacts.js" async></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/landing-js/form.min.js" async></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/landing-js/main.min.js" async></script>
		
		<!-- Facebook Pixel Code -->
		<script async>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1561691214139374');
			fbq('track', "PageView");
		</script>
		<noscript>
			<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1561691214139374&ev=PageView&noscript=1"	/>
		</noscript>
		<!-- End Facebook Pixel Code -->
				
		<!-- Код тега ремаркетинга Google -->
		<script type="text/javascript" async>
			/* <![CDATA[ */
			var google_conversion_id = 932375060;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js" async>
		</script>
		<noscript>
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/932375060/?value=0&amp;guid=ON&amp;script=0"/>
			</div>
		</noscript>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38871820 = new Ya.Metrika({
                    id:38871820,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38871820" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38871820 = new Ya.Metrika({
                    id:38871820,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38871820" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!--Google analytics-->

<script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75589514-4', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

  /* Accurate bounce rate by time */
if (!document.referrer ||
     document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'Новый посетитель', location.pathname);
 }, 15000);</script>

<!--Google analytics-->
    <?php wp_footer(); ?>
	</body>
</html>