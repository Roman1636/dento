<?php 
	/*Template Name: Page-Landing*/
?>

<?php get_header('landing'); ?>

<?php
	if (have_rows('content_repeater')) {

	  while (have_rows('content_repeater')) {
	    the_row();

	    if (have_rows('landing_repeater_item')) {

	      while (have_rows('landing_repeater_item')) {      	
			the_row();         
			$layout = get_row_layout();

			switch ($layout) {
	        	case 'intro': 
	        		echo get_template_part('landing-templates/intro');
	        		break;
	    		case 'capabilities': 
	        		echo get_template_part('landing-templates/capabilities');
	        		break;
        		case 'information': 
	        		echo get_template_part('landing-templates/information');
	        		break;
        		case 'advantages_disadvantages': 
	        		echo get_template_part('landing-templates/advantages_disadvantages');
	        		break;
        		case 'brands': 
	        		echo get_template_part('landing-templates/brands');
	        		break;
        		case 'price': 
	        		echo get_template_part('landing-templates/price');
	        		break;
        		case 'cost': 
	        		echo get_template_part('landing-templates/cost');
	        		break;
        		case 'testimonials': 
	        		echo get_template_part('landing-templates/testimonials');
	        		break;
        		case 'professional': 
	        		echo get_template_part('landing-templates/professional');
	        		break;
        		case 'reception': 
	        		echo get_template_part('landing-templates/reception');
	        		break;
        		case 'partners': 
	        		echo get_template_part('landing-templates/partners');
	        		break;
        		case 'specialists': 
	        		echo get_template_part('landing-templates/specialists');
	        		break;
	        } // end switch	        
	      } 
	    } 
	  } 
	} 
?>

<?php get_footer('landing'); ?>