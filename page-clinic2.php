<?php
/*   
Template Name: Акция
*/
?>
<?php get_header('page'); ?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
	bcn_display();
}
?>
</div>

<div class="content-main sidebord">
<div class="sidebordText" style="width:960px;">


<div class="all_specialist all_spec_clin">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<!-- post navigation -->
	<?php else: ?>
	<!-- no posts found -->
	<?php endif; ?>

<?php 	$args = array(
		'post_parent' => $post->ID,
		'post_type' => 'page',
		'orderby' => array( 'menu_order' => 'DESC', 'title' => 'ASC' ),
		'posts_per_page' => 100
	);
	$subpages = new WP_query($args);
	if ($subpages->have_posts()) :
		$output = '<ul>';
		while ($subpages->have_posts()) : $subpages->the_post();?>



</p>
<a href="<?php the_permalink(); ?>">Подробнее</a>
<a href="#contact_form_pop" class="fancybox-inline zapis">Записаться на прием</a>
<div style="display:none" class="fancybox-hidden">
<div id="contact_form_pop"> 
<h2>ON-LINE ЗАПИСЬ К ВРАЧУ</h2><br/>              
[contact-form-7 id="1986" title="Запись на прием"]
</div>
</div>

</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
	<div class="clear"></div>
</div>
</div>
	
	</div>
</div>

<?php get_footer(); ?>