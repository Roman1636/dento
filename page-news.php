<?php
/*   
Template Name: Список новостей
*/
?>
<?php get_header('page'); ?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
	bcn_display();
}
?>
</div>
<div class="content-main sidebord">
<div class="sidebordText">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<!-- post navigation -->
	<?php else: ?>
	<!-- no posts found -->
	<?php endif; ?>
	<?php $news_list_all = new WP_Query(array('post_type' => 'all_news', 'order' => 'ASC')) ?>
<?php if ( $news_list_all->have_posts() ) : ?>
     
<?php while ( $news_list_all->have_posts() ) : $news_list_all->the_post(); ?>
<div class="list_acticles">
<?php
 the_post_thumbnail('full'); ?>
<h3><?php the_title(); ?></h3>
<?php the_excerpt(); ?>
<a class="moreView" href="<?php the_permalink(); ?>">Подробнее</a>
</div>
<?php endwhile; ?>
     
<?php else: ?>
<?php endif; ?>

</div>
	<div class="sidebordRight">
	  <ul style="padding:0;">
  <div class="textwidget">
 <?php $args = array( 'post_type' => array('page'),
                    'meta_key' => 'услуги и цены',
                    'orderby' => 'meta_value_num',
                    'order' => 'ASC'); ?>
<?php $page_index = new WP_Query($args); ?>

<?php if ( $page_index->have_posts() ) : while ( $page_index->have_posts() ) : $page_index->the_post(); ?>

<li class="page_item page_item_has_children">
  <a href="<?php the_permalink(); ?>"><?php the_title(); ?>
  </a>
	<?php 
	$children = wp_list_pages('title_li=&child_of='.get_the_ID().'&echo=0&depth=1');
  if ($children) { ?>
 
<ul class="children">
	<?php echo $children; ?>
</ul>

  <?php } ?>
</li>
 
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
</div>
  </ul>
  <a href="<?php echo home_url(); ?>/konsultatsiya" class="bottomkons">Консультация</a>
  <div class="banner_menu">
  <?php if(!dynamic_sidebar( 'banner_menu' )): ?>
<?php endif; ?>  
</div>
	</div>
</div>

<?php get_footer(); ?>