<?php
/*   
Template Name: Специалисты
*/
?>
<?php get_header('page'); ?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
	bcn_display();
}
?>
</div>

<div class="content-main">

<div class="all_specialist">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<!-- post navigation -->
	<?php else: ?>
	<!-- no posts found -->
	<?php endif; ?>
<?php $args = array( 'post_type' => array('page'),
                    'meta_key' => 'специалист',
					'orderby' => array('title' => 'ASC' ), //'orderby' => array( 'menu_order' => 'DESC', 'title' => 'ASC' ),
                    'posts_per_page' => 300 ); ?>
<?php $page_index = new WP_Query($args); ?>

<?php if ( $page_index->have_posts() ) : while ( $page_index->have_posts() ) : $page_index->the_post(); ?>

<div class="page-list_people">
<?php the_post_thumbnail('full'); ?>
<h2><?php the_title(); ?></h2>
<p>
<?php  
$content = get_the_content();
preg_match_all('#\<em\>(.*)\<\/em\>#', $content, $result);
echo $result[0][0];
?>
</p>
<a href="<?php the_permalink(); ?>">Подробнее</a>
<a href="#contact_form_pop" class="fancybox-inline zapis">Записаться на прием</a>
<div style="display:none" class="fancybox-hidden">
<div id="contact_form_pop"> 
<h2>ON-LINE ЗАПИСЬ К ВРАЧУ</h2><br/>              
[contact-form-7 id="1986" title="Запись на прием"]
</div>
</div>
</div>
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
	<div class="clear"></div>
</div>
</div>

<?php get_footer(); ?>