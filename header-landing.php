<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<meta name="description" content="Имплантация от лучших мировых брендов. Москва, сеть стоматологических клиник Дента-Эль. тел.: 8(495) 6625885, 09:00-21:00. Звоните!"> 
		<title><?php wp_title(); ?></title>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<?php wp_head(); ?>
	</head>
	<body>
		<div class="container">
			<div class="row" id="line1">
				<div class="col-xs-12 col-md-4">
					<div id="mainLogo"></div>
				</div>
				<div class="col-xs-12 col-md-4">
					<div id="adressContainer">
						<p id="adressHead">Прием во всех филиалах ежедневно с 09:00 до 21:00</p>
						<div id="adressOuter">
							<select id="adress">
								<option></option>
								<option>м. б-р адмирала Ушакова (ул. Южнобутовская, д. 8)</option>
								<option>м. Аэропорт (ул. Черняховского, д. 2)</option>
								<option>м. Братиславская (ул. Братиславская, д. 26)</option>
								<option>м. Измайловская (Первомайская ул, д. 42, вход со стороны ул.5-я Парковая)</option>
								<option>м. Каховская (Симферопольский бул., д. 17, корп. 1)</option>
								<option>м. Отрадное(ул. Хачатуряна, д. 12, корп. 1)</option>
								<option>м. "Пл. Ильича", "Римская" ( ул. Сергия Радонежского, д. 5/2 стр. 1)</option>
								<option>м. Полежаевская (Хорошевское ш., д. 80)</option>
								<option>м. Речной вокзал (ул. Фестивальная, д. 4)</option>
								<option>м. Сходненская (ул. Героев Панфиловцев, д. 1)</option>
								<option>м. Университет (Университетский пр. 4)</option>
								<option>м. Цветной бульвар (ул. Садовая-Самотечная, д. 13)</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<div id="telContainer">
						<p id="telLine1">Единый контактный центр</p>
						<p id="telLine2">8&nbsp;(495)&nbsp;662&nbsp;58&nbsp;85</p>
					</div>
				</div>
			</div>
		</div>
		
		
		