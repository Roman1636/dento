<?php
/*
 Single Post Template: single-custom-tamplate-for-posts
 Description: This part is optional, but helpful for describing the Post Template
 */
?>
<?php get_header('page'); ?>
    <div class="breadcrumb">
        <?php
        if(function_exists('bcn_display'))
        {
            bcn_display();
        }
        ?>
    </div>

    <div class="content-main sidebord">
    <div class="sidebordText">
        <div class="all_specialist all_spec_clin">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <?
                $post_id = get_the_ID();
                $desc =get_field('описание');
                $adr = get_field('адрес');
                $phone = get_field('телефон');
                $hours = get_field('режим_работы');

                ?>
                <h1><?php the_title(); ?></h1>
                <div class="node_page clinic_page">
                    <div class="content">
                        <? if(!empty($desc)){?><p class="metro"><?=$desc?></p><? }?>
                        <? if(!empty($adr)){?><p><strong>Адрес:</strong> <a href="https://yandex.ru/maps/?mode=search&text=<?=$adr?>" target="_blank"><?=$adr?></a></p><? }?>
                        <? if(!empty($phone)){?> <p><strong>Телефон:</strong> <a href="tel:<?=$phone?>"><?=$phone?></a></p><? }?>
                        <? if(!empty($hours)){?><p><strong>Режим работы:</strong> <?=$hours?></p><? }?>
                    </div>
                </div>





                <?
                $val_int = get_the_id();
                $dir_doctor = 1;


                global $wpdb;


                $querystr = "
            SELECT p.ID, p.post_title, pmeta1.meta_value, pmeta2.meta_value
            FROM {$wpdb->posts} p
            JOIN {$wpdb->postmeta} pmeta1
                ON p.ID = pmeta1.post_id 
                AND pmeta1.meta_key LIKE 'repter_%_klinika' AND pmeta1.meta_value = $val_int
            JOIN {$wpdb->postmeta} pmeta2
                ON pmeta1.post_id = pmeta2.post_id 
                AND pmeta2.meta_key LIKE 'repter_%_ddoctor' AND pmeta2.meta_value = $dir_doctor
            WHERE 
            p.post_status = 'publish' 
            AND 
            p.post_type = 'doctor'
            
           ORDER BY p.post_title ASC 
           
        ";
                $pageposts = $wpdb->get_results($querystr, OBJECT);


                /*
                 *
                 *
                 *
                 * JOIN {$wpdb->postmeta} klinika
                       ON p.ID = klinika.post_id AND klinika.meta_key LIKE 'repter_%_klinika'
                    JOIN {$wpdb->postmeta} ddoctor
                       ON p.ID = ddoctor.post_id AND ddoctor.meta_key LIKE 'repter_%_ddoctor'

                    SELECT     DISTINCT key1.post_id
                    FROM        $wpdb->postmeta key1
                    INNER JOIN  $wpdb->postmeta key2
                                    on key2.post_id = key1.post_id
                                    and key2.meta_key LIKE 'repter_%_klinika' and key2.meta_value = $val_int
                    WHERE       key1.meta_key LIKE 'repter_%_ddoctor'
                                    and key1.meta_value = $dir_doctor



                SELECT p.ID id, p.post_title title, country.meta_value country, region.meta_value region
                FROM {$wpdb->posts} p
                JOIN {$wpdb->postmeta} country
                    ON p.ID = country.post_id AND country.meta_key = 'search_country'
                JOIN {$wpdb->postmeta} region
                    ON p.ID = region.post_id AND region.meta_key = 'search_region'
                WHERE
                    p.post_status = 'publish'
                    AND p.post_type = 'films'
                ORDER BY p.post_title ASC
        */

                ?>

                <?php if( $pageposts ): ?>

                    <? $result = array();?>
                    <?php foreach( $pageposts as $doctor ): ?>


                        <?
                        $proverka = array();
                        if( have_rows('repter',$doctor->ID) ):
                            while ( have_rows('repter',$doctor->ID) ) : the_row();
                                $val = get_sub_field('ddoctor');
                                $wk = get_sub_field_object('klinika');
                                $id = $wk['value'];

                                $proverka[$id] = $val;
                            endwhile;
                        endif;

                        if($proverka[$post_id]!=1):
                            continue;

                        endif;

                        $main_title = "<h2>Главный врач</h2>";

                        //$doctor->ID = $doctor->post_id;
                        $photo = get_field('фото', $doctor->ID);
                        $title = get_the_title( $doctor->ID );
                        if( have_rows('repter',$doctor->ID) ):
                            while ( have_rows('repter',$doctor->ID) ) : the_row();
                                $doljn = '<p><em>'.get_sub_field('должность').'</p>';
                                break;
                            endwhile;
                        endif;

                        if( current_user_can('administrator')):
                            $adm = '<a href="'.get_edit_post_link( $doctor->ID ).'"> Ред. </a>';
                        endif;

                        $html = '
                    <div class="page-list_people">
                    <img width="150" height="150" src="'.$photo.'" class="attachment-full wp-post-image" alt="Cпециалист '.get_the_title( $doctor->ID ).'">
                    <h2>'.$title.'</h2>
                    '.$doljn.'
                    <a href="'.get_permalink( $doctor->ID ).'">Подробнее</a> '.$adm.'

                    <a href="#contact_form_pop" class="fancybox-inline zapis">Записаться на прием</a>
                    <div style="display:none" class="fancybox-hidden">
                        <div id="contact_form_pop">
                            <h2>ON-LINE ЗАПИСЬ К ВРАЧУ</h2><br>
                            [contact-form-7 id="1986" title="Запись на прием"]
                        </div>
                    </div>
                </div>';

                        $result[$title] = $html;
                        ?>
                    <?php endforeach; ?>
                    <?php print $main_title;?>
                    <?php foreach( $result as $doctor ): ?>
                        <? print $doctor;?>
                    <?php endforeach; ?>

                <?php endif; ?>

                <? wp_reset_postdata();?>

                <?
                $val_int = get_the_id();






                $dir_doctor = 0;
                $querystr = "
            SELECT p.ID, p.post_title, pmeta1.meta_value, pmeta2.meta_value
            FROM {$wpdb->posts} p
            JOIN {$wpdb->postmeta} pmeta1
                ON p.ID = pmeta1.post_id 
                AND pmeta1.meta_key LIKE 'repter_%_klinika' AND pmeta1.meta_value = $val_int
            JOIN {$wpdb->postmeta} pmeta2
                ON pmeta1.post_id = pmeta2.post_id 
                AND pmeta2.meta_key LIKE 'repter_%_ddoctor' AND pmeta2.meta_value = $dir_doctor
            WHERE 
            p.post_status = 'publish' 
            AND 
            p.post_type = 'doctor'
            
           ORDER BY p.post_title ASC
        ";



                $pageposts = $wpdb->get_results($querystr, OBJECT);

                ?>



                <div class="clearfix"></div>

                <?php if( $pageposts ): ?>

                    <? $result = array();?>
                    <?php foreach( $pageposts as $doctor ): ?>


                        <?
                        $proverka = array();
                        if( have_rows('repter',$doctor->ID) ):
                            while ( have_rows('repter',$doctor->ID) ) : the_row();
                                $val = get_sub_field('ddoctor');
                                $wk = get_sub_field_object('klinika');
                                $id = $wk['value'];

                                $proverka[$id] = $val;
                            endwhile;
                        endif;

                        if($proverka[$post_id]==1):
                            continue;

                        endif;

                        $main_title = "<h2>Врачи</h2>";

                        $photo = get_field('фото', $doctor->ID);
                        $title = get_the_title( $doctor->ID );
                        if( have_rows('repter',$doctor->ID) ):
                            while ( have_rows('repter',$doctor->ID) ) : the_row();
                                $doljn = '<p><em>'.get_sub_field('должность').'</p>';
                                break;
                            endwhile;
                        endif;

                        if( current_user_can('administrator')):
                            $adm = '<a href="'.get_edit_post_link( $doctor->ID ).'"> Ред. </a>';
                        endif;

                        $html = '
                    <div class="page-list_people">
                    <img width="150" height="150" src="'.$photo.'" class="attachment-full wp-post-image" alt="Cпециалист <?=get_the_title( $doctor->ID )?>">
                    <h2>'.$title.'</h2>
                    '.$doljn.'
                    <a href="'.get_permalink( $doctor->ID ).'">Подробнее</a> '.$adm.'

                    <a href="#contact_form_pop" class="fancybox-inline zapis">Записаться на прием</a>
                    <div style="display:none" class="fancybox-hidden">
                        <div id="contact_form_pop">
                            <h2>ON-LINE ЗАПИСЬ К ВРАЧУ</h2><br>
                            [contact-form-7 id="1986" title="Запись на прием"]
                        </div>
                    </div>
                </div>';

                        $result[$title] = $html;
                        ?>
                    <?php endforeach; ?>
                    <? print $main_title;?>
                    <?php foreach( $result as $doctor ): ?>
                        <? print $doctor;?>
                    <?php endforeach; ?>

                <?php endif; ?>
            <?php endwhile; ?>
                <!-- post navigation -->
            <?php else: ?>
                <!-- no posts found -->
            <?php endif; ?>



        </div>
    </div>

    <div class="sidebordRight">
        <ul style="padding:0;">
            <div class="textwidget">
                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/">Терапевтическая стоматология  </a>

                    <ul class="children2">
                        <li class="page_item page-item-973 page_item_has_children"><a href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/">Все о терапевтической стоматологии</a></li>
                        <li class="page_item page-item-973 page_item_has_children"><a href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/restavratsiya-zubov/">Реставрация зубов</a></li>
                        <li class="page_item page-item-974 page_item_has_children"><a href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/vosstanovlenie-zubov/">Восстановление зубов</a></li>
                        <li class="page_item page-item-975 page_item_has_children"><a href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/lechenie-kariesa/">Лечение кариеса</a></li>
                        <li class="page_item page-item-976 page_item_has_children"><a href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/lechenie-pulpita/">Лечение пульпита</a></li>
                        <li class="page_item page-item-977 page_item_has_children"><a href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/lechenie-periodontita/">Лечение периодонтита</a></li>
                        <li class="page_item page-item-978 page_item_has_children"><a href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/podgotovka-k-protezirovaniyu/">Подготовка к протезированию</a></li>
                    </ul>

                </li>


                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/parodontologiya/">Пародонтология  </a>

                    <ul class="children2">
                        <li class="page_item page-item-920 page_item_has_children"><a href="http://dentol.ru/uslugi/parodontologiya/">Все о пародонтологии</a></li>
                        <li class="page_item page-item-920 page_item_has_children"><a href="http://dentol.ru/uslugi/parodontologiya/lechenie-parodontoza/">Лечение пародонтоза</a></li>
                        <li class="page_item page-item-921 page_item_has_children"><a href="http://dentol.ru/uslugi/parodontologiya/lechenie-desen/">Лечение десен</a></li>
                        <li class="page_item page-item-922 page_item_has_children"><a href="http://dentol.ru/uslugi/parodontologiya/hirurgicheskaya-parodontologiya/">Хирургическая пародонтология</a></li>
                        <li class="page_item page-item-923 page_item_has_children"><a href="http://dentol.ru/uslugi/parodontologiya/lechenie-apparatom-vektor/">Лечение аппаратом Вектор</a></li>
                        <li class="page_item page-item-924 page_item_has_children"><a href="http://dentol.ru/uslugi/parodontologiya/shinirovanie-zubov/">Шинирование зубов</a></li>
                    </ul>

                </li>


                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/kompyuternaya-tomografiya/">Компьютерная томография  </a>

                    <ul class="children2">
                        <li class="page_item page-item-908 page_item_has_children"><a href="http://dentol.ru/uslugi/kompyuternaya-tomografiya/">Все о компьютерной томографии</a></li>
                        <li class="page_item page-item-908 page_item_has_children"><a href="http://dentol.ru/uslugi/kompyuternaya-tomografiya/rengenologicheskoe-otdelenie/">Ренгенологическое отделение</a></li>
                    </ul>

                </li>


                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/stomatologiya-pod-narkozom/">Стоматология под наркозом  </a>
                    <ul class="children2">
                        <li class="page_item page-item-908 page_item_has_children"><a href="http://dentol.ru/uslugi/stomatologiya-pod-narkozom/">Все о стоматологии под наркозом</a></li></ul>

                </li>





                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/ortopediya-2/">Ортопедия  </a>
                    <ul class="children2">
                        <li class="page_item page-item-908 page_item_has_children"><a href="http://dentol.ru/uslugi/ortopediya-2/">Все об ортопедии</a></li></ul>
                </li>

                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/esteticheskaya-stomatologiya/">Эстетическая стоматология  </a>

                    <ul class="children2">
                        <li class="page_item page-item-952 page_item_has_children"><a href="http://dentol.ru/uslugi/esteticheskaya-stomatologiya/">Все об эстетической стоматологии</a></li>
                        <li class="page_item page-item-952 page_item_has_children"><a href="http://dentol.ru/uslugi/esteticheskaya-stomatologiya/otbelivanie-zubov/">Отбеливание зубов</a></li>
                        <li class="page_item page-item-953 page_item_has_children"><a href="http://dentol.ru/uslugi/esteticheskaya-stomatologiya/kompyuternaya-tehnologiya-cerec/">Компьютерная технология CEREC</a></li>
                        <li class="page_item page-item-954"><a href="http://dentol.ru/uslugi/esteticheskaya-stomatologiya/keramicheskie-viniryi/">Керамические виниры</a></li>
                        <li class="page_item page-item-955 page_item_has_children"><a href="http://dentol.ru/uslugi/esteticheskaya-stomatologiya/keramicheskie-vkladki/">Керамические вкладки</a></li>
                        <li class="page_item page-item-956 page_item_has_children"><a href="http://dentol.ru/uslugi/esteticheskaya-stomatologiya/keramicheskie-koronki/">Керамические коронки</a></li>
                        <li class="page_item page-item-957 page_item_has_children"><a href="http://dentol.ru/uslugi/esteticheskaya-stomatologiya/e`steticheskie-restavratsii/">Эстетические реставрации</a></li>
                    </ul>

                </li>


                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/hirurgiya-implantatsiya/">Хирургия / имплантация  </a>
                    <ul class="children2">
                        <li class="page_item page-item-908 page_item_has_children"><a href="http://dentol.ru/uslugi/hirurgiya-implantatsiya/">Все о хирургии и имплантации</a></li></ul>

                </li>


                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/detskaya-stomatologiya/">Детская стоматология  </a>

                    <ul class="children2">
                        <li class="page_item page-item-758 page_item_has_children"><a href="http://dentol.ru/uslugi/detskaya-stomatologiya/">Все о детской стоматологии</a></li>
                        <li class="page_item page-item-758 page_item_has_children"><a href="http://dentol.ru/uslugi/detskaya-stomatologiya/lechenie-molochnyih-zubov/">Лечение молочных зубов</a></li>
                        <li class="page_item page-item-759 page_item_has_children"><a href="http://dentol.ru/uslugi/detskaya-stomatologiya/vosstanovlenie-molochnyih-zubov/">Восстановление молочных зубов</a></li>
                        <li class="page_item page-item-760 page_item_has_children"><a href="http://dentol.ru/uslugi/detskaya-stomatologiya/profilaktika-kariesa/">Профилактика кариеса</a></li>
                        <li class="page_item page-item-761 page_item_has_children"><a href="http://dentol.ru/uslugi/detskaya-stomatologiya/germetizatsiya-fissur/">Герметизация фиссур</a></li>
                        <li class="page_item page-item-762"><a href="http://dentol.ru/uslugi/detskaya-stomatologiya/plastika-uzdechki/">Пластика уздечки</a></li>
                        <li class="page_item page-item-763"><a href="http://dentol.ru/uslugi/detskaya-stomatologiya/ispravlenie-prikusa/">Исправление прикуса</a></li>
                    </ul>

                </li>


                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/gigiena-i-profilaktika-zubov/">Гигиена и профилактика  </a>

                    <ul class="children2">
                        <li class="page_item page-item-752 page_item_has_children"><a href="http://dentol.ru/uslugi/gigiena-i-profilaktika-zubov/">Все о гигиене и профилактике</a></li>
                        <li class="page_item page-item-752 page_item_has_children"><a href="http://dentol.ru/uslugi/gigiena-i-profilaktika-zubov/professionalnaya-gigiena-zubov/">Профессиональная гигиена зубов</a></li>
                        <li class="page_item page-item-753 page_item_has_children"><a href="http://dentol.ru/uslugi/gigiena-i-profilaktika-zubov/ultrazvukovaya-chistka-zubov-air-flow/">Ультразвуковая чистка зубов Air Flow</a></li>
                        <li class="page_item page-item-754"><a href="http://dentol.ru/uslugi/gigiena-i-profilaktika-zubov/remoterapiya-zubov-v-seti-stomatologi/">Ремотерапия зубов в сети стоматологий Дента-Эль</a></li>
                        <li class="page_item page-item-755"><a href="http://dentol.ru/uslugi/gigiena-i-profilaktika-zubov/ftorirovanie-zubov/">Фторирование зубов</a></li>
                    </ul>

                </li>


                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/protezirovanie/">Протезирование зубов  </a>

                    <ul class="children2">
                        <li class="page_item page-item-1008 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/">Все о протезировании зубов</a></li>
                        <li class="page_item page-item-1008 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/bezmetallovaya-keramika/">Безметалловая керамика</a></li>
                        <li class="page_item page-item-1009 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/metallokeramika/">Металлокерамика</a></li>
                        <li class="page_item page-item-1010 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/metallokeramika-na-zolote/">Металлокерамика на золоте</a></li>
                        <li class="page_item page-item-1011 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/byugelnyie-protezyi/">Бюгельные протезы</a></li>
                        <li class="page_item page-item-1012 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/byugelnyie-protezyi-na-zamkah/">Бюгельные протезы на замках</a></li>
                        <li class="page_item page-item-1013 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/plastinochnyie-protezyi/">Пластиночные протезы</a></li>
                        <li class="page_item page-item-1014 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/neylonovyie-protezyi/">Нейлоновые протезы</a></li>
                        <li class="page_item page-item-1015 page_item_has_children"><a href="http://dentol.ru/uslugi/protezirovanie/protezirovanie-na-implantatah/">Протезирование на имплантатах</a></li>
                    </ul>

                </li>


                <li class="page_item page_item_has_children">
                    <a class="main-item" href="javascript:void(0);" tabindex="1" href="http://dentol.ru/uslugi/ispravlenie-prikusa/">Исправление прикуса  </a>

                    <ul class="children2">
                        <li class="page_item page-item-2171"><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/">Все об исправлении прикуса</a></li>
                        <li class="page_item page-item-2171"><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/lechenie-na-nesemnoy-ortodonticheskoy-tehnike/">Лечение на несъемной ортодонтической технике</a></li>
                        <li class="page_item page-item-2168"><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/lechenie-na-semnoy-ortodonticheskoy-tehnike/">Лечение на съемной ортодонтической технике</a></li>
                        <li class="page_item page-item-818 page_item_has_children"><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/konsultatsiya-i-diagnostika/">Консультация и диагностика</a></li>
                        <li class="page_item page-item-821 page_item_has_children"><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/dopolnitelnyie-prisposobleniya-i-man/">Дополнительные приспособления и манипуляции</a></li>
                        <li class="page_item page-item-822 page_item_has_children"><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/gigienicheskie-protseduryi/">Гигиенические процедуры</a></li>
                        <li class="page_item page-item-823 page_item_has_children"><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/snyatie-breket-sistem/">Снятие брекет-систем</a></li>
                        <li class="page_item page-item-824 page_item_has_children"><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/retentsionnyiy-period/">Ретенционный период</a></li>
                    </ul>

                </li>
                <!-- post navigation -->


            </div>
        </ul>

        <a href="<?php echo home_url(); ?>/konsultatsiya" class="bottomkons">Консультация</a>
        <div class="banner_menu">
            <?php if(!dynamic_sidebar( 'banner_menu' )): ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="clear"></div>

<?php get_footer(); ?>