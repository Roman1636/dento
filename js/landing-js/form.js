var tooths = [];
$(document).ready(function () {
	
	//При клике по номеру зуба
	$(".tooth").on("click", function () {
		if ($(this).hasClass("toothSelected")) {
			$(this).removeClass("toothSelected");
		} else {
			$(this).addClass("toothSelected");
		}
		toothsCount();
	});
	
	//При клике на крестик у выбранного зуба
	$("#form1RightInner").on("click", ".toothElement span", function () {
		$(this).parent().detach();
		$("#tooth-" + parseInt($(this).parent().text())).removeClass("toothSelected");
		toothsCount();
	});

});

//Функция перерасчета выбранных зубов
function toothsCount () {
	$("#selectedTooths").html("");
	var selectedTooths = "";
	tooths = [];
	$.each($(".tooth"), function () {
		if ($(this).hasClass("toothSelected")) {
			var index = $(this).attr("id").replace("tooth-", "");
			tooths.push(index);
			selectedTooths += "<div class='toothElement'>" + index + "<span>✕</span></div>";
		}
	});
	if (selectedTooths == "") {
		$("#selectedTooths").html("<p class='emptyText'>Пока ничего не выбрано</p>");
	} else {
		$("#selectedTooths").html(selectedTooths);
	}
}