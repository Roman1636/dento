var message = "";
var messageInfo = "";
$(document).ready(function () {
	//Стилизуем селект выбора адреса
	$("#adress").select2({
		placeholder: "Адреса 12 клиник"
	});
	
	//Выравниваем блоки
	equalHeight(".line3BlockText");
	equalHeight(".line9BlockText");
	equalHeight(".line14BlockText");
	equalHeight(".line15BlockText2");
	equalHeight(".line15BlockText3");
	
	//Создаем маски полей ввода номера телефона
	$(".clientPhone, #form1ClientPhone, .line17FormInput").mask("+7 (999) 999-99-99");
	
	//Убираем красную рамку с полей ввода при фокусе
	$("#line2FormName, #line2FormPhone, #mailBox .clientName, #mailBox .clientPhone, #form1ClientName, #form1ClientPhone, .line13formInput, .line17FormInput, .line17FormTextarea").on("focus", function () {
		$(this).removeClass("inpError");
	});
	
	//Верхняя форма - кнопка "Записаться на бесплатный прием"
	$("#line2FormBtn").on("click", function () {
		var inpError = 0;
		if ($("#line2FormName").val() == "") {
			$("#line2FormName").addClass("inpError");
			inpError++;
		}
		if ($("#line2FormPhone").val() == "") {
			$("#line2FormPhone").addClass("inpError");
			inpError++;
		}
		if ( inpError == 0) {
			message = "<h1>Заявка со страницы 'Имплантация'</h1>";
			message += "<p><b>Имя клиента:</b> "+$("#line2FormName").val()+"</p>";
			message += "<p><b>Телефон клиента:</b> "+$("#line2FormPhone").val()+"</p>";
			message += "<p><b>Примечание:</b> Нажата кнопка 'Записаться на бесплатный прием'</p>";
			mailStart();
		}
	});
	
	//Выбор типа имплантации - кнопка "Выбрать"
	$(".line7Btn1, .line7Btn2").on("click", function () {
		messageInfo = "<p><b>Выбор клиента:</b> "+$(this).parent().find(".line7Head").text()+"</p>";
		$("#mailBoxContainer").css("top", (($(window).height() - $("#mailBoxContainer").outerHeight()) / 3)+"px");
		$("#shadowBox, #mailBoxContainer").fadeIn();
	});
	
	//Калькулятор - кнопка "Узнать стоимость лечения"
	$("#form1Submit").on("click", function () {
		var inpError = 0;
		if ($("#form1ClientName").val() == "") {
			$("#form1ClientName").addClass("inpError");
			inpError++;
		}
		if ($("#form1ClientPhone").val() == "") {
			$("#form1ClientPhone").addClass("inpError");
			inpError++;
		}
		if (inpError == 0) {
			toothsCount()
			message = "<h1>Заявка расчета стоимости лечения со страницы 'Имплантация'</h1>";
			message += "<p><b>Имя клиента:</b> "+$("#form1ClientName").val()+"</p>";
			message += "<p><b>Телефон клиента:</b> "+$("#form1ClientPhone").val()+"</p>";
			message += "<p><b>Выбранные зубы:</b></p><ul>";
			$.each(tooths, function (i, val) {
				message += "<li>" + val + "</li>";
			});
			message += "</ul>";
			mailStart();
		}
	
	});	
	
	//Блок со скидкой - кнопка "Записаться на прием"	
	$("#line12 .formBtn").on("click", function () {
		messageInfo = "<p><b>Примечание:</b> Нажата кнопка 'Записаться на прием'</p>";
		$("#mailBoxContainer").css("top", (($(window).height() - $("#mailBoxContainer").outerHeight()) / 3)+"px");
		$("#shadowBox, #mailBoxContainer").fadeIn();
	});		
	
	//Блок со скидкой - кнопка "Записаться на бесплатный прием, получить скидку и рассрочку"
	$(".line13formBtn").on("click", function () {
		var inpError = 0;
		if ($("#line13form .clientName").val() == "") {
			$("#line13form .clientName").addClass("inpError");
			inpError++;
		}
		if ($("#line13form .clientPhone").val() == "") {
			$("#line13form .clientPhone").addClass("inpError");
			inpError++;
		}
		if ( inpError == 0) {
			message = "<h1>Заявка со страницы 'Имплантация'</h1>";
			message += "<p><b>Имя клиента:</b> "+$("#line13form .clientName").val()+"</p>";
			message += "<p><b>Телефон клиента:</b> "+$("#line13form .clientPhone").val()+"</p>";
			message += "<p><b>Примечание:</b> Нажата кнопка 'Записаться на бесплатный прием, получить скидку и рассрочку'</p>";
			mailStart();
		}
	});
	
	//Блок выбора специалиста - кнопка "Записаться на прием"	
	$(".line15Block .formBtn").on("click", function () {
		messageInfo = "<p><b>Выбранный специалист:</b></p>";
		messageInfo += "<p>"+$(this).parent().find(".line15BlockText1").text()+"</p>";
		messageInfo += "<p>"+$(this).parent().find(".line15BlockText2").text()+"</p>";
		messageInfo += "<p>"+$(this).parent().find(".line15BlockText3").text()+"</p>";
		$("#mailBoxContainer").css("top", (($(window).height() - $("#mailBoxContainer").outerHeight()) / 3)+"px");
		$("#shadowBox, #mailBoxContainer").fadeIn();
	});	
	
	//Нижняя форма - кнопка "Задать вопрос"
	$(".line17FormBtn").on("click", function () {
		var inpError = 0;
		if ($(".line17FormInput").val() == "") {
			$(".line17FormInput").addClass("inpError");
			inpError++;
		}
		if ($(".line17FormTextarea").val() == "") {
			$(".line17FormTextarea").addClass("inpError");
			inpError++;
		}
		if ( inpError == 0) {
			message = "<h1>Вопрос от клиента со страницы 'Имплантация'</h1>";
			message += "<p><b>Телефон клиента:</b> "+$(".line17FormInput").val()+"</p>";
			message += "<p><b>Вопрос:</b></p><p>"+$(".line17FormTextarea").val()+"</p>";
			mailStart();
		}
	});	
	
	
	//Отправка заявок
	$("#mailBox .formBtn").on("click", function () {
		var inpError = 0;
		if ($("#mailBox .clientName").val() == "") {
			$("#mailBox .clientName").addClass("inpError");
			inpError++;
		}
		if ($("#mailBox .clientPhone").val() == "") {
			$("#mailBox .clientPhone").addClass("inpError");
			inpError++;
		}
		if ( inpError == 0) {
			message = "<h1>Заявка со страницы 'Имплантация'</h1>";
			message += "<p><b>Имя клиента:</b> "+$("#mailBox .clientName").val()+"</p>";
			message += "<p><b>Телефон клиента:</b> "+$("#mailBox .clientPhone").val()+"</p>";
			message += messageInfo;
			mailStart();
		}		
	});
	
	
	//Нижняя форма - кнопка "Политика конфиденциальности"
	$(".line17Text1 span").on("click", function () {
		$("#popupHead").text("Политика конфиденциальности");
		$("#popupText").html("Сайт уважает и соблюдает законодательство РФ. Также мы уважаем Ваше право и соблюдаем конфиденциальность при заполнении, передаче и хранении ваших конфиденциальных сведений.<br><br>Мы запрашиваем Ваши персональные данные исключительно для информирования об оказываемых услугах сайта.<br><br>Персональные данные - это информация, относящаяся к субъекту персональных данных, то есть, к потенциальному покупателю. В частности, это фамилия, имя и отчество, дата рождения, адрес, контактные реквизиты (телефон, адрес электронной почты), семейное, имущественное положение и иные данные, относимые Федеральным законом от 27 июля 2006 года № 152-ФЗ «О персональных данных» (далее – «Закон») к категории персональных данных.<br><br>Если Вы разместили Ваши контактные данных на сайте, то Вы автоматически согласились на обработку данных и дальнейшую передачу Ваших контактных данных менеджерам нашего сайта.<br><br>В случае отзыва согласия на обработку своих персональных данных мы обязуемся удалить Ваши персональные данные в срок не позднее 3 рабочих дней.");
		popupShow("620px");
	});
	
	//Закрываем всплывающее окно
	$("#popupCloseBtn").on("click", function () {
		$("#shadowBox, #popupBoxContainer").fadeOut();
	});
	$("#mailCloseBtn").on("click", function () {
		$("#shadowBox, #mailBoxContainer").fadeOut();
	});
	
	//Блок  картинок отзывов звезд - кнопка "Увеличить"
	$(".imgZoomBtn").on("click", function () {
		var imgSrc = $(this).parent().parent().find(".line10RightImg").attr("src");
		$("#popupHead").text("");
		$("#popupText").html("<img  onload='popupShow()' src='"+imgSrc.replace(".jpg", "")+"_fullsize.jpg' style='max-width: "+(parseInt($(window).width())-60)+"px; max-height: "+(parseInt($(window).height())-91)+"px;'>");
	});
	
	//Блок выбора специалиста - кнопка "Посмотреть документы"
	$(".line15BlockText4 span").on("click", function () {
		var index =  $(".line15BlockText4 span").index($(this)) + 1;
		$("#popupHead").text("");
		$("#popupText").html("<img onload='popupShow()' src='css/images/line15_doc"+index+".jpg' style='max-width: "+(parseInt($(window).width())-60)+"px; max-height: "+(parseInt($(window).height())-91)+"px;'>");
	});

	
	
	$(".line15Block .formBtn").on("click", function () {
		
	});
});

//При изменении размеров окна
$( window ).resize(function() {
	//Выравниваем блоки
	equalHeight(".line3BlockText");
	equalHeight(".line9BlockText");
	equalHeight(".line14BlockText");
	equalHeight(".line15BlockText2");
	equalHeight(".line15BlockText3");
});

//Показываем всплывающее окно
function popupShow(maxWidth) {
	if (maxWidth) {
		$("#popupBox").css("max-width", maxWidth);
	} else {
		$("#popupBox").css("max-width", $(window).width());
	}
	$("#popupBox").css("max-height", $(window).height());
	$("#popupBoxContainer").css("top", (($(window).height() - $("#popupBoxContainer").outerHeight()) / 3)+"px");
	$("#shadowBox, #popupBoxContainer").fadeIn();	
}

//Отправка заявки
function mailStart() {
	$.ajax({
		type: "POST",
		url: "php/send.php",
		data: {
			message: message,
			email: email
		},
		success: function() {
			window.location.href = "http://thankyou.dentol-plus.ru";
		},
		error: function() {
			$("#popupHead").text("Внимание!");
			$("#popupText").html("При отправке произошла ошибка!");
			$("#popupBox").css("max-width", "400px");
			$("#popupBox").css("max-height", $(window).height());
			$("#popupBoxContainer").css("top", (($(window).height() - $("#popupBoxContainer").outerHeight()) / 3)+"px");
			$("#mailBoxContainer").fadeOut();
			$("#shadowBox, #popupBoxContainer").fadeIn();					
		}
	});
}

//Выравниваем блоки 
function equalHeight (blockClass) {
	var maxHeight = 0;
	$.each($(blockClass), function () {
		if ($(this).height() > maxHeight) {
			maxHeight = $(this).height();
		}
	});
	$(blockClass).height(maxHeight+"px");
}