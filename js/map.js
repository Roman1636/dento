ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
            center: [55.751574, 37.573856],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        }),

    // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject({
           
        });

    myMap.geoObjects
        .add(myGeoObject)
		
		
        .add(new ymaps.Placemark([55.544066, 37.540632], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/b-r-admirala-ushakova">Бульвар адмирала Ушакова</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
		
        .add(new ymaps.Placemark([55.65431, 37.763728], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-v-marino-m-bratislavsk">Стоматология в Марьино, м. Братиславская</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
        .add(new ymaps.Placemark([55.856573, 37.485268], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-v-rayone-m-rechnoy-vokzal">Стоматология м. Речной вокзал</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
        .add(new ymaps.Placemark([55.697067, 37.55381], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-v-rayone-m-universitet">Стоматология м. Университет</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
        .add(new ymaps.Placemark([55.857422, 37.59604], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-v-rayone-stantsii-m-otrad">Стоматология м. Отрадное</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
        .add(new ymaps.Placemark([55.792736, 37.787075], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-na-m-izmaylovskaya-ul-per">Стоматология м. Измайловская</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
        .add(new ymaps.Placemark([55.778512, 37.522881], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-na-m-polezhaevskaya">Стоматология м. Полежаевская</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
		
	
		
		.add(new ymaps.Placemark([55.652024, 37.606496], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-u-m-kahovskaya">Стоматология м. Каховская</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
		.add(new ymaps.Placemark([55.852834, 37.438071], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-u-m-shodnenskaya">Стоматология м. Сходненская</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
		
		
		
		
		
		
		
		
		
		
		
		.add(new ymaps.Placemark([55.774234, 37.617824], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-u-m-tsvetnoy-bulvar">Стоматология м. Цветной бульвар</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
		
		.add(new ymaps.Placemark([55.80145, 37.5327], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-na-metro-aeroport-moskva">Стоматология м. Аэропорт</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }))
		
		
		
		
		
		
		
		
        .add(new ymaps.Placemark([55.747196, 37.674076], {
            balloonContent: '<a href="http://dentol.ru/set-klinik/stomatologiya-u-m-pl-ilicharimskaya">Стоматология у м. "Пл. Ильича"/"Римская"</a>'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'http://dentol.ru/logo_ya.gif',
            iconImageSize: [40, 52],
            iconImageOffset: [-3, -42]
        }));
}
