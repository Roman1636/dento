<div class="container">
	<div class="row" id="line11">
		<div class="col-xs-12">
			<?php if(get_sub_field('landing_professional_title')) : ?>
				<div class="lineHead">
				 	<?php the_sub_field('landing_professional_title'); ?>
				</div>
			<?php endif; ?>
		</div>	
	</div>
	<?php if( have_rows('landing_professional_repeater') ) : ?>
		<?php $i = 1; ?>
		<?php while( have_rows('landing_professional_repeater') ) : the_row(); ?>
			<?php if( $i % 2 ) : ?> <!-- if number is even - then block will be replaced on the left and vice versa -->
				<div class="row line11Block" id="line11Block<?php echo $i; ?>">
					<div class="col-xs-12 col-md-4">
						<div class="line11BlockImg line11BlockImg<?php echo $i; ?> hidden-xs hidden-sm"></div>
					</div>
					<div class="col-xs-12 col-md-8">
						<div class="line11BlockHead">
							<?php the_sub_field('landing_professional_repeater_title'); ?>
						</div>
						<div class="line11BlockImg line11BlockImg<?php echo $i; ?> hidden-md hidden-lg"
							style="background-image: url('<?php the_sub_field('landing_professional_repeater_img'); ?>');">
						</div>
						<div class="line11BlockText">
							<?php the_sub_field('landing_professional_repeater_text'); ?>
						</div>
					</div>
				</div>
			<?php else : ?>
				<div class="row line11Block" id="line11Block<?php echo $i; ?>">
					<div class="col-xs-12 col-md-8">
						<div class="line11BlockHead">
							<?php the_sub_field('landing_professional_repeater_title'); ?>
						</div>
						<div class="line11BlockImg line11BlockImg<?php echo $i; ?> hidden-md hidden-lg"
							style="background-image: url('<?php the_sub_field('landing_professional_repeater_img'); ?>');">
						</div>
						<div class="line11BlockText">
							<?php the_sub_field('landing_professional_repeater_text'); ?>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="line11BlockImg line11BlockImg<?php echo $i; ?> hidden-xs hidden-sm"></div>
					</div>
				</div>
			<?php endif; ?>
			<?php ++$i; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>