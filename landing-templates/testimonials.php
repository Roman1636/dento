<div class="container">
	<div class="hr">
		<div class="hrLine"></div>
		<div class="hrImg"></div>
	</div>
</div>
<div class="container testimonials-block">
	<div class="row" id="line10">
		<div class="col-xs-12">
			<?php if(get_sub_field('landing_testimonials_title')) : ?>
				<div class="lineHead line3Head">
					<div class="lineHead">
					 	<?php the_sub_field('landing_testimonials_title'); ?>
					</div>
				</div>
			<?php endif; ?>
		</div>	
	</div>
	<?php if( have_rows('landing_testimonials_repeater') ) : ?>
		<?php $i = 1; ?>
		<?php while( have_rows('landing_testimonials_repeater') ) : the_row(); ?>
			<div class="row line10Block">
				<div class="hidden-xs hidden-sm col-md-3">
					<div class="line10LeftImg line10LeftImg<?php echo $i; ?>"
						style="background: 
						url('<?php the_sub_field('landing_testimonials_repeater_left_img'); ?>');">
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 hidden-md hidden-lg">
					<div class="line10RightImgContainer">
						<img class="line10RightImg" 
						src="<?php the_sub_field('landing_testimonials_repeater_right_img'); ?>">
					</div>
					<div class="imgZoom">
						<span class="imgZoomBtn">
							<span>УВЕЛИЧИТЬ</span>
						</span>
					</div>					
				</div>
				<div class="col-xs-12 col-sm-8 col-md-6">
					<div class="line10Text">
						<?php the_sub_field('landing_testimonials_repeater_text'); ?>
						<br><br>
						<?php the_sub_field('landing_testimonials_repeater_description'); ?>
					</div>
				</div>
				<div class="hidden-xs hidden-sm col-md-3">
					<div class="line10RightImgContainer">
						<img class="line10RightImg" 
							src="<?php the_sub_field('landing_testimonials_repeater_right_img'); ?>">
					</div>
					<div class="imgZoom">
						<span class="imgZoomBtn">
							<span>УВЕЛИЧИТЬ</span>
						</span>
					</div>
				</div>
			</div>	
			<?php ++$i; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
<div class="container">
	<div class="hr">
		<div class="hrLine"></div>
		<div class="hrImg"></div>
	</div>
</div>
