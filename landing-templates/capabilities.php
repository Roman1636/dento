<div class="container capabilities-block">
	<div class="row" id="line3">
		<div class="col-xs-12">
			<?php if(get_sub_field('landing_capabilities_title')) : ?>
				<div class="lineHead line3Head">
					<?php the_sub_field('landing_capabilities_title'); ?>
				</div>
			<?php endif; ?>
		</div>
		 
		<?php if( have_rows('landing_capabilities_repeater') ) : ?>
			<?php $i = 1; ?>
			<?php while( have_rows('landing_capabilities_repeater') ) : the_row(); ?>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<!-- line3Block -->
					<div class="line3Block">
						<div class="line3BlockImg line3BlockImg<?php echo $i; ?>" 
							style="background-image: url('<?php the_sub_field('landing_capabilities_repeater_image'); ?>');">
						</div>
						<p class="line3BlockHead blockHead"><?php the_sub_field('landing_capabilities_repeater_title'); ?></p>
						<p class="line3BlockText blockText"><?php the_sub_field('landing_capabilities_repeater_content'); ?></p>
					</div>
					<!-- END:line3Block -->
				</div>
				<?php ++$i; ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>
