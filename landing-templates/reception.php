<div id="line12Container"
	style="background-image: url('<?php the_sub_field('landing_reception_bg'); ?>');">
	<div class="container">
		<div class="row" id="line12">
		<div class="col-xs-12">
			<div class="line12Text1">
				<?php if(get_sub_field('landing_reception_title')) : ?>
					<?php the_sub_field('landing_reception_title'); ?>
				<?php endif; ?>
			</div>
			<div class="line12Text2">
				<p class="line12NewPrice">
					<span class="line12NewPrice1">
						<?php if(get_sub_field('landing_reception_new_price')) : ?>
							<?php the_sub_field('landing_reception_new_price'); ?>
						<?php endif; ?>
					</span>
					<span class="line12NewPrice2">
						<?php if(get_sub_field('landing_reception_currency')) : ?>
							<?php the_sub_field('landing_reception_currency'); ?>
						<?php endif; ?>
					</span>
				</p> 
				вместо 
				<span class="line12OldPrice">
					<?php if(get_sub_field('landing_reception_old_price')) : ?>
						<?php the_sub_field('landing_reception_old_price'); ?>
					<?php endif; ?>
					<?php if(get_sub_field('landing_reception_currency')) : ?>
						<?php the_sub_field('landing_reception_currency'); ?>
					<?php endif; ?>
				</span>
			</div>
			<div class="line12Text3">По телефону:</div>
			<div class="line12Text4">8&nbsp;(495)&nbsp;662&nbsp;58&nbsp;85</div>
			<div class="formBtn">Записаться на прием</div>
		</div>
		</div>
	</div>
</div>


