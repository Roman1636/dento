<div class="container">
	<div class="row" id="line13">
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			<?php if(get_sub_field('landing_partners_title')) : ?>
				<div class="line13Text1">
					<?php the_sub_field('landing_partners_title'); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="col-xs-12">
			<div class="line13Img">
				<img src="<?php the_sub_field('landing_partners_img'); ?>" alt="">
			</div>
		</div>
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			<div class="line13Text2">
				<?php if(get_sub_field('landing_partners_text')) : ?>
					<?php the_sub_field('landing_partners_text'); ?>
				<?php endif; ?>	
			</div>
		</div>
	</div>
	<div class="row" id="line13form">
		<?php if(get_sub_field('landing_partners_form')) : ?>
			<?php the_sub_field('landing_partners_form'); ?>
		<?php endif; ?>
	</div>
</div>