<div id="line8Container"
	style="background-image: url('<?php the_sub_field('landing_cost_bg'); ?>');">
	<div class="container">
		<div class="row" id="line8">
			<div class="col-xs-12">
				<div class="lineHead">
					<?php if(get_sub_field('landing_cost_title')) : ?>
						<?php the_sub_field('landing_cost_title'); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div id="form1Left">
					<p id="mouthTopText" class="mouthText">Верхняя челюсть</p>
					<p id="mouthBottomText" class="mouthText">Нижняя челюсть</p>
					<div class="tooth" id="tooth-11">11</div>
					<div class="tooth" id="tooth-12">12</div>
					<div class="tooth" id="tooth-13">13</div>
					<div class="tooth" id="tooth-14">14</div>
					<div class="tooth" id="tooth-15">15</div>
					<div class="tooth" id="tooth-16">16</div>
					<div class="tooth" id="tooth-17">17</div>
					<div class="tooth" id="tooth-18">18</div>
					<div class="tooth" id="tooth-21">21</div>
					<div class="tooth" id="tooth-22">22</div>
					<div class="tooth" id="tooth-23">23</div>
					<div class="tooth" id="tooth-24">24</div>
					<div class="tooth" id="tooth-25">25</div>
					<div class="tooth" id="tooth-26">26</div>
					<div class="tooth" id="tooth-27">27</div>
					<div class="tooth" id="tooth-28">28</div>
					<div class="tooth" id="tooth-31">31</div>
					<div class="tooth" id="tooth-32">32</div>
					<div class="tooth" id="tooth-33">33</div>
					<div class="tooth" id="tooth-34">34</div>
					<div class="tooth" id="tooth-35">35</div>
					<div class="tooth" id="tooth-36">36</div>
					<div class="tooth" id="tooth-37">37</div>	
					<div class="tooth" id="tooth-38">38</div>	
					<div class="tooth" id="tooth-41">41</div>
					<div class="tooth" id="tooth-42">42</div>
					<div class="tooth" id="tooth-43">43</div>
					<div class="tooth" id="tooth-44">44</div>
					<div class="tooth" id="tooth-45">45</div>
					<div class="tooth" id="tooth-46">46</div>
					<div class="tooth" id="tooth-47">47</div>				
					<div class="tooth" id="tooth-48">48</div>				
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div id="form1RightInner" class="lineForm">
					<?php if(get_sub_field('landing_cost_form')) : ?>
						<?php the_sub_field('landing_cost_form'); ?>
					<?php endif; ?>
					<p id="form1RightPhone">Или перезвоните по телефону:<br><span> 8 (495) 662-58-85</span></p>
				</div>
			</div>
		</div>
	</div>
</div>