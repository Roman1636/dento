<div class="container">
	<div class="row" id="line7">
		<?php if( have_rows('landing_price_repeater') ) : ?>
			<?php $i = 1; ?>
			<?php while( have_rows('landing_price_repeater') ) : the_row(); ?>
				<div class="col-xs-12 col-md-4">
					<div class="line7BlockContainer">
						<div class="line7Block<?php echo ($i == 2? '2': '1'); ?>">
							<div class="line7Head line7Head<?php echo ($i == 2? '2': '1'); ?>">
								<?php the_sub_field('landing_price_repeater_service'); ?>
							</div>
							<div class="line7HeadImg<?php echo ($i == 2? '2': '1'); ?> line7HeadImg11"
							style="background-image: url('<?php the_sub_field('landing_price_repeater_img'); ?>')">				
							</div>
							<p class="line7OldPrice">
								<?php the_sub_field('landing_price_repeater_old_price'); ?> 
								<span>
									<?php the_sub_field('landing_price_repeater_currency'); ?>.
								</span>
							</p>
							<p class="line7CurPrice">
								<?php the_sub_field('landing_price_repeater_new_price'); ?> 
								<span>
									<?php the_sub_field('landing_price_repeater_currency'); ?>.
								</span>
							</p>
							<div class="line7BodyImg<?php echo ($i == 2? '2': '1'); ?> line7BodyImg11"></div>
							<div class="line7Btn<?php echo ($i == 2? '2': '1'); ?>">Выбрать</div>
						</div>
					</div>
				</div>
				<?php ++$i; ?>
			<?php endwhile; ?>
		<?php endif; ?>


		<!-- <div class="col-xs-12 col-md-4">
			<div class="line7BlockContainer">
				<div class="line7Block2">
					<div class="line7Head line7Head2">Имплантация Аstra</div>
					<div class="line7HeadImg2 line7HeadImg12"></div>
					<p class="line7OldPrice">49 500 <span>р.</span></p>
					<p class="line7CurPrice">37 500 <span>р.</span></p>
					<div class="line7BodyImg2 line7BodyImg12"></div>
					<div class="line7Btn2">Выбрать</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="line7BlockContainer">
				<div class="line7Block1">
					<div class="line7Head line7Head1">Имплантация Cortex</div>
					<div class="line7HeadImg1 line7HeadImg13"></div>
					<p class="line7OldPrice">29 900 <span>р.</span></p>
					<p class="line7CurPrice">19 500 <span>р.</span></p>
					<div class="line7BodyImg1 line7BodyImg13"></div>
					<div class="line7Btn1">Выбрать</div>
				</div>
			</div>
		</div> -->

	</div>
</div>