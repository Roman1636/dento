<div class="container">
	<div class="row" id="line5">
		<div class="col-xs-12 col-md-6">
			<?php if(get_sub_field('landing_disadvantages_title')) : ?>
				<p class="line5LeftHead">
					<?php the_sub_field('landing_disadvantages_title'); ?>
				</p>
			<?php endif; ?>
			<?php if( have_rows('landing_disadvantages_repeater') ) : ?>
				<?php while( have_rows('landing_disadvantages_repeater') ) : the_row(); ?>
					<div class="minusBlock">
						<p class="minusBlockText">
							<?php the_sub_field('landing_disadvantages_repeater_text'); ?>
						</p>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="col-xs-12 col-md-6">
			<div id="plusHeadContainer">
				<div id="plusHeadOuter">
					<?php if(get_sub_field('landing_advantages_title')) : ?>
						<p class="line5LeftHead" id="plusHead">
							<?php the_sub_field('landing_advantages_title'); ?>				
						</p>
					<?php endif; ?>
				</div>	
			</div>
			<?php if( have_rows('landing_advantages_repeater') ) : ?>
				<?php while( have_rows('landing_advantages_repeater') ) : the_row(); ?>
					<div class="plusBlock">
						<p class="plusBlockText">
							<?php the_sub_field('landing_advantages_repeater_text'); ?>
						</p>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>				
		<div class="col-xs-12 col-md-6">
		</div>
	</div>
</div>