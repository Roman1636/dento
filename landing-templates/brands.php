<div class="container">
	<div class="hr">
		<div class="hrLine"></div>
		<div class="hrImg"></div>
	</div>
</div>
<div class="container">
	<div class="row" id="line6">
		<div class="col-xs-12">
			<?php if(get_sub_field('landing_brands_title')) : ?>
				<div class="lineHead">
					<?php the_sub_field('landing_brands_title'); ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if( have_rows('landing_brands_repeater') ) : ?>
			<?php while( have_rows('landing_brands_repeater') ) : the_row(); ?>
				<div class="col-xs-12 col-md-4">
					<div class="line6Block">
						<div class="line6Img line6Img1"
							style="background-image: url('<?php the_sub_field('landing_brands_repeater_img'); ?>')">
						</div>
						<p class="line6OldPrice">
							<?php the_sub_field('landing_brands_repeater_old_price'); ?>
						</p>
						<p class="line6CurPrice">
							<?php the_sub_field('landing_brands_repeater_new_price'); ?>
						</p>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>
<div class="container">
	<div class="hr">
		<div class="hrLine"></div>
		<div class="hrImg"></div>
	</div>
</div>