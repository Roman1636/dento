<div id="line2Container" style="background-image: url('<?php the_sub_field('landing_intro_bg'); ?>')">
	<div class="container">
		<div class="row" id="line2">
			<div class="col-sm-12 col-md-6">
				<div id="line2Left">
					<img src="<?php the_sub_field('landing_intro_img'); ?>">
				</div>
			</div>
			<div class="line2RightTextBlock  hidden-xs hidden-sm">
				<?php if(get_sub_field('landing_intro_title')) : ?>
					<p class="line2RightText1">
						<?php the_sub_field('landing_intro_title'); ?>
					</p>
				<?php endif; ?>
				<?php if(get_sub_field('landing_intro_content')) : ?>
					<p class="line2RightText2">
						<?php the_sub_field('landing_intro_content'); ?>
					</p>
				<?php endif; ?>
				<p class="line2RightText3">от   
					<span class="oldPrice">
						<?php if(get_sub_field('landing_intro_old_price')) : ?>
							<?php the_sub_field('landing_intro_old_price'); ?>
						<?php endif; ?>
					</span>   
					<span class="newPrice">
						<?php if(get_sub_field('landing_intro_new_price')) : ?>
							<?php the_sub_field('landing_intro_new_price'); ?>
						<?php endif; ?>
					</span>   
					<?php if(get_sub_field('landing_intro_currency')) : ?>
						<?php the_sub_field('landing_intro_currency'); ?>
					<?php endif; ?>
				</p>
			</div>
			<div class="col-sm-12 col-md-6">
				<?php 
					if(get_sub_field('landing_intro_form')) :
						the_sub_field('landing_intro_form');	
					endif;
				?>
			</div>
		</div>
	</div>
</div>