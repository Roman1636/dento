<div class="container specialists-block">
	<div class="row" id="line15">
		<div class="col-xs-12">
			<div class="lineHead">
				<?php if(get_sub_field('landing_specialists_title')) : ?>
					<?php the_sub_field('landing_specialists_title'); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php if( have_rows('landing_specialists_repeater') ) : ?>
			<?php $i = 1; ?>
			<?php while( have_rows('landing_specialists_repeater') ) : the_row(); ?>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<!-- line15Block -->
					<div class="line15Block">
						<div class="line15BlockImg line15BlockImg<?php echo $i; ?>"
							style="background-image: url('<?php the_sub_field('landing_specialists_repeater_img'); ?>');">		
						</div>
						<div class="line15BlockText1"><?php the_sub_field('landing_specialists_repeater_metro'); ?></div>
						<div class="line15BlockText2"><?php the_sub_field('landing_specialists_repeater_name'); ?></div>
						<div class="line15BlockText3"><?php the_sub_field('landing_specialists_repeater_info'); ?></div>
						<div class="formBtn">Записаться на прием</div>
						<div class="line15BlockText4"><span>ПОСМОТРЕТЬ ДОКУМЕНТЫ</span></div>
						<img src="<?php the_sub_field('landing_specialists_repeater_document'); ?>" class="specialists-block__certificate">
					</div>
					<!-- END:line15Block -->
				</div>
				<?php ++$i; ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>