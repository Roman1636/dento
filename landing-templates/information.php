<div id="line4Container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php if(get_sub_field('landing_information_title')) : ?>
					<div class="lineHead" id="line4Head">
						<?php the_sub_field('landing_information_title'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="row" id="line4">
			<div class="col-xs-12">
					<!-- line4BlockContainer -->
					<div id="line4BlockContainer">
						<div id="line4Img" 
							style="background-image: url('<?php the_sub_field('landing_information_bg'); ?>');">
						</div>
						<?php if( have_rows('landing_information_repeater') ) : ?>
							<?php $i = 1; ?>
							<?php while( have_rows('landing_information_repeater') ) : the_row(); ?>
								<!-- line4Block -->
								<div class="line4Block line4Block<?php echo $i; ?>">
									<p class="line4BlockHead blockHead">
										<?php the_sub_field('landing_information_repeater_title'); ?>
									</p>
									<p class="line4BlockText blockText">
										<?php the_sub_field('landing_information_repeater_content'); ?>
									</p>
								</div>
								<!-- END:line4Block -->
							<?php ++$i; ?>
						<?php endwhile; ?>
					</div>
					<!-- END:line4BlockContainer -->
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>