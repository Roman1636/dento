<!DOCTYPE html>
<html>
 <head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KW5FZM');</script>
<!-- End Google Tag Manager -->
   <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php bloginfo('name'); wp_title(); ?></title>
  <?php wp_head(); ?>
  <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
<link rel="icon" href="/favicon.png" type="image/x-icon">
 <!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter34284420 = new Ya.Metrika({
                    id:34284420,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/34284420" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter26711265 = new Ya.Metrika({
                    id:26711265,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/26711265" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->



<!--Open Graph Facebook-->

<?php if (have_posts()):while(have_posts()):the_post(); endwhile; endif;?>  

<!-- постоянные значения -->  

<meta property="fb:admins" content="https://www.facebook.com/antohaliptonik" />  
  
<!-- если это статья -->  

<?php if (is_single()) { ?>  
<meta property="og:url" content="<?php the_permalink(); ?> "/>  
<meta property="og:title" content="<?php single_post_title(''); ?>" />  
<meta property="og:description" 
         content="<?php echo strip_tags(get_the_excerpt($post->ID)); ?>" />  
<meta property="og:type" content="article" />  
<meta property="og:image" 
content="<?php if (function_exists('wp_get_attachment_thumb_url')) {echo 
wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID)); } ?>" />  
  
<!-- если это любая другая страница -->  

<?php } else { ?>  
<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />  
<meta property="og:description" 
       content="<?php bloginfo('description'); ?>" />  
<meta property="og:type" content="website" />  
<meta property="og:image" content="http://dentol.ru/wp-content/uploads/2015/09/24_02.jpg" /> 
<?php } ?> 

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=KvlUsjbUCiKXEDjZ6Jvjzd/z*UgnSgkcKVbaj2AcKscxLGzz5X1ObN9/nJnYxUgGANy1/QKq4Luq1rSDer0B8ffkt5szSDO/HacqKbS1N1E*mpg6elnYToL7kB0PkFmymHU6fmFLmNLeqZNsfxDCxxUaB6htL9WA351KddpEzMo-&pixel_id=1000030612';</script>
 </head>
 <body class="<?php body_class(); ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KW5FZM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
 
  <div class="main">
<div id="head">
		<div class="logo_head divhead"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url') ?>/images/logo5.png"></a></div>
		<div class="time_head divhead"><!--p><?php if(!dynamic_sidebar( 'online_reception' )): ?>
<?php endif; ?> </p-->
		<p class="time_head_rej"><?php echo get_option('my_work'); ?></p>
<p class="phone_head divhead" style="font-size: 12px;color: #9e3a5a;padding-top: 4px;">Единый контактный центр</p><span ><a href="tel:+74956625885" style="color: #008fc6;font-size: 23px;text-decoration: none;"><?php echo get_option('my_phone'); ?></a></span>
		</div>
		<!-- <div class="phone_head divhead"><p>Единый контактный центр</p><span><a href="tel:+74956625885"><?php echo get_option('my_phone'); ?></a></span></div> -->
		<div class="search_head divhead">
		
            	<form method="get" action="<?php echo home_url(); ?>">
                	<input name="s" class="search-txt" type="text" value="Поиск..." onBlur="if(this.value=='')this.value='Поиск...'" onFocus="if(this.value=='Поиск...')this.value=''" />
                    <input type="image" src="<?php bloginfo('template_url') ?>/images/search-bg.png" name="go" />
                </form>
				
		<p><a class="map" href="/set-klinik">Карта клиник</a></p></div>
		<div class="cal_head divhead">
		<a class="calcul" href="/kalkulyator">Расчет стоимости</a>
		<?php if(!dynamic_sidebar( 'icons_header' )): ?>
<?php endif; ?> 

		</div>
		
<div class="branches2">
<a href="http://semeynaya.ru/">Сеть медицинских центров</a>

<a  href="http://beauty.semeynaya.ru/">Клиника красоты в Семейной</a>

<a  href="http://medsurgeon.ru/">Практический центр хирургии</a></div>


		</div>
			
	</div>
	</div>
<div id="menu" class="fixpage">
	
		<?php wp_nav_menu(array(
    'theme_location' => 'header_menu',
    'container' => '',
    'menu_class' => ''
)); ?>	
	</div>
	<div class="main">