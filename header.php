<!DOCTYPE html>
<html>
 <head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KW5FZM');</script>
<!-- End Google Tag Manager -->
 <meta name='yandex-verification' content='7ce08e286bcef9a4' />
 <meta name='yandex-verification' content='7634448f10a8c621' />
<meta name="google-site-verification" content="id_rCG193ERQwEI9XNkBkNu5Nl-2GYYN-wporqYVWgY" />
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=NC82yLb2lyveji1f05KZuOofQ8vSlgVCZ6UyLoYQaTy4Qa1dB7IN772OmRG6x3yFjiUXW1WhN40oXRneHwn3QPOvwZwH*DMmM0Jjko7Zm2CGSb1*viG8Du6f7D8ZB1qu4RV*r57t7ur3PM/KxJYgwAUBcq4lROPVnM*G9RAzh3A-';</script>
<meta name="google-site-verification" content="fSp4cVkXo-1iONb8Vk7v17PSDtGaGUo7X1kKg4AUXME" />
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php bloginfo('name'); wp_title(); ?></title>
  <?php wp_head(); ?>
  <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
<link rel="icon" href="/favicon.png" type="image/x-icon">
  <script type="text/javascript">
  
   $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  

	$(window).scroll(function(){
        if ($(window).scrollTop() > 368) {
            $('#menu').addClass('header-fixed');
        }
        else {
            $('#menu').removeClass('header-fixed');
        }
    });
  </script>
<meta name="google-site-verification" content="xGPXKvUh3dGyTwCCCEq1Vsciivf_BYG68-fMgpn2GtA" />
<!--Google analytics-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75589514-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

  /* Accurate bounce rate by time */
if (!document.referrer ||
     document.referrer.split('/')[2].indexOf(location.hostname) != 0)
 setTimeout(function(){
 ga('send', 'event', 'Новый посетитель', location.pathname);
 }, 15000);</script>

<!--Google analytics-->

<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=260282,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=KvlUsjbUCiKXEDjZ6Jvjzd/z*UgnSgkcKVbaj2AcKscxLGzz5X1ObN9/nJnYxUgGANy1/QKq4Luq1rSDer0B8ffkt5szSDO/HacqKbS1N1E*mpg6elnYToL7kB0PkFmymHU6fmFLmNLeqZNsfxDCxxUaB6htL9WA351KddpEzMo-&pixel_id=1000030612';</script>
</head>
 <body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KW5FZM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <div class="main">
<div id="head">
		<div class="logo_head divhead"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url') ?>/images/logo5.png"></a></div>
		<div class="time_head divhead"><!--p><?php if(!dynamic_sidebar( 'online_reception' )): ?>
<?php endif; ?> </p-->
		<p class="time_head_rej"><?php echo get_option('my_work'); ?></p>
<p class="phone_head divhead" style="font-size: 12px;color: #9e3a5a;padding-top: 4px;">Единый контактный центр</p><span ><a href="tel:+74956625885" style="color: #008fc6;font-size: 23px;text-decoration: none;"><?php echo get_option('my_phone'); ?></a></span>
		</div>
		<!-- <div class="phone_head divhead"><p>Единый контактный центр</p><span><a href="tel:+74956625885"><?php echo get_option('my_phone'); ?></a></span></div> -->
		<div class="search_head divhead">
		

            	<form method="get" action="<?php echo home_url(); ?>">
                	<input name="s" class="search-txt" type="text" value="Поиск..." onBlur="if(this.value=='')this.value='Поиск...'" onFocus="if(this.value=='Поиск...')this.value=''" />
                    <input type="image" src="<?php bloginfo('template_url') ?>/images/search-bg.png" name="go" />
                </form>
				
		<p><a class="map" href="/set-klinik">Карта клиник</a></p></div>
		<div class="cal_head divhead">
<?php if(!dynamic_sidebar( 'icons_header' )): ?>
	<?php endif; ?> 


		</div>
		
<div class="branches2">

<a href="http://semeynaya.ru/">Сеть медицинских центров</a>

<a  href="http://beauty.semeynaya.ru/">Клиника красоты в Семейной</a>

<a  href="http://medsurgeon.ru/">Практический центр хирургии</a></div>

	</div>
	

	