<?php
/*   
Template Name: Список акций
*/
?>
<?php get_header('page'); ?>

<div class="breadcrumb">
<?php
if(function_exists('bcn_display'))
{
	bcn_display();
}
?>
</div>
<div class="content-main sidebord">
<div class="sidebordText">

<section id="primary" class="site-content">
<div id="content" role="main">
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post();?>
<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>


<div class="entry">
<?php the_content(); ?>

 <p class="postmetadata"><?php comments_popup_link( 'Нет комментариев', '1 комментарий', '% комментариев', 'Комментарии закрыты');?></p>
</div>

<?php endwhile; else: ?>


<?php endif; ?>
</div>
</section>
</div>
	<div class="sidebordRight">
	  <ul style="padding:0;">
  <div class="textwidget">
 <?php $args = array( 'post_type' => array('page'),
                    'meta_key' => 'услуги и цены',
                    'orderby' => 'meta_value_num',
                    'order' => 'ASC'); ?>
<?php $page_index = new WP_Query($args); ?>

<?php if ( $page_index->have_posts() ) : while ( $page_index->have_posts() ) : $page_index->the_post(); ?>

<li class="page_item page_item_has_children">
  <a class="main-item" href="javascript:void(0);" tabindex="1" href="<?php the_permalink(); ?>"><?php the_title(); ?>
  </a>
	<?php 
	$children = wp_list_pages('title_li=&child_of='.get_the_ID().'&echo=0&depth=1');
  if ($children) { ?>
 
<ul class="children2">
	<?php echo $children; ?>
</ul>

  <?php } ?>
</li>
 
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
</div>
  </ul>
  <a href="<?php echo home_url(); ?>/konsultatsiya" class="bottomkons">Консультация</a>
  <div class="banner_menu">
  <?php if(!dynamic_sidebar( 'banner_menu' )): ?>
<?php endif; ?>  
</div>
	</div>
</div>

<?php get_footer(); ?>