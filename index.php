<?php get_header(); ?>
<?php /* $slider = new WP_Query(array('post_type' => 'slider', 'order' => 'ASC')) ?>
<?php if ( $slider->have_posts() ) : ?>
       <div class="slider">
        <div class="flexslider">
          <ul class="slides">
<?php while ( $slider->have_posts() ) : $slider->the_post(); ?>
<?php $url = get_post_meta(get_the_ID(),'url',true); ?>
            <a href=" <?php echo $url; ?>"><li>
            	<div class="slide-content">
                    <?php the_content(); ?>
                </div>
  	    	    <?php the_post_thumbnail('full'); ?>
  	    	</li></a>
<?php endwhile; ?>
          </ul>
        </div>
      </div>
<?php else: ?>
<?php endif; */?>
<?php if ( function_exists( 'easingslider' ) ) { easingslider( 2127 ); } ?>
</div>

	<div id="menu">
	
		<?php wp_nav_menu(array(
    'theme_location' => 'header_menu',
    'container' => '',
    'menu_class' => ''
)); ?>	
	</div>
	<div class="main"> 
	<div id="uslugy">
	<div class="b-carousel">
		<div class="b-carousel-button-left"></div>
		<div class="b-carousel-button-right"></div> 
		<div class="h-carousel-wrapper"> 
			<div class="h-carousel-items">
<div class="b-carousel-block">
<a href="http://dentol.ru/uslugi/hirurgiya-implantatsiya/"><img width="70" height="70" src="http://i2.wp.com/dentol.ru/wp-content/uploads/2015/08/07.png?fit=70%2C70" class="attachment-full wp-post-image" alt="" /><span>Хирургия / имплантация</span></a>
</div>
<div class="b-carousel-block">
<a href="http://dentol.ru/uslugi/protezirovanie/"><img width="70" height="70" src="http://i1.wp.com/dentol.ru/wp-content/uploads/2015/08/08.png?fit=70%2C70" class="attachment-full wp-post-image" alt="" /><span>Протезирование зубов</span></a>
</div>
<div class="b-carousel-block">
<a href="http://dentol.ru/uslugi/parodontologiya/"><img width="70" height="70" src="http://i2.wp.com/dentol.ru/wp-content/uploads/2015/08/03.png?fit=70%2C70" class="attachment-full wp-post-image" alt="" /><span>Пародонтология</span></a>
</div>
<div class="b-carousel-block">
<a href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/"><img width="70" height="70" src="http://i1.wp.com/dentol.ru/wp-content/uploads/2015/09/09.png?fit=70%2C70" class="attachment-full wp-post-image" alt="" /><span>Терапевтическая стоматология</span></a>
</div>
<div class="b-carousel-block">
<a href="http://dentol.ru/uslugi/detskaya-stomatologiya/"><img width="70" height="70" src="http://i1.wp.com/dentol.ru/wp-content/uploads/2015/08/01.png?fit=70%2C70" class="attachment-full wp-post-image" alt="" /><span>Детская стоматология</span></a>
</div>
<div class="b-carousel-block">
<a href="http://dentol.ru/uslugi/gigiena-i-profilaktika-zubov/"><img width="70" height="70" src="http://i1.wp.com/dentol.ru/wp-content/uploads/2015/08/04.png?fit=70%2C70" class="attachment-full wp-post-image" alt="" /><span>Гигиена и профилактика</span></a>
</div>
<div class="b-carousel-block">
<a href="http://dentol.ru/uslugi/ortopediya-2/"><img width="70" height="70" src="http://dentol.ru/wp-content/uploads/2016/04/ortopedia.png" class="attachment-full wp-post-image" alt="" /><span>Ортопедия</span></a>
</div>
<!-- post navigation -->
			</div>
		</div>
	</div>
	<div class="clear"></div>
	
	
	</div>
	<div id="menu2">
	<div>
		<h2></h2>
			<ul class="m2-1">
				

		
<li><a href="http://dentol.ru/uslugi/gigiena-i-profilaktika-zubov/">Гигиена и профилактика</a></li>
   
<li><a href="http://dentol.ru/uslugi/detskaya-stomatologiya/">Детская стоматология</a></li>
   
<li><a href="http://dentol.ru/uslugi/ispravlenie-prikusa/">Исправление прикуса</a></li>
   
<li><a href="http://dentol.ru/uslugi/protezirovanie/">Протезирование зубов</a></li>
   
<li><a href="http://dentol.ru/uslugi/parodontologiya/">Пародонтология</a></li>

<li><a href="http://dentol.ru/uslugi/ortopediya-2/">Ортопедия</a></li>
   
<!-- post navigation -->
		

			</ul>
		</div>
		<div>
		<h2></h2>
			<ul class="m2-2">
				
<?php $args = array( 'post_type' => array('page'),
                    'meta_key' => 'menu_service_right',
                    'orderby' => 'meta_value_num',
                    'order' => 'ASC',
                    'posts_per_page' => 5 ); ?>
<?php $page_index = new WP_Query($args); ?>

<?php if ( $page_index->have_posts() ) : while ( $page_index->have_posts() ) : $page_index->the_post(); ?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
   
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
				
			</ul>
		</div>
		<div class="clear"></div>
	</div>

	<div id="pirm">
	<ul>
	
	<?php $args = array( 'post_type' => array('page'),
                    'meta_key' => 'advantages',
                    'orderby' => 'meta_value_num',
                    'order' => 'ASC',
                    'posts_per_page' => 4 ); ?>
<?php $page_index = new WP_Query($args); ?>

<?php if ( $page_index->have_posts() ) : while ( $page_index->have_posts() ) : $page_index->the_post(); ?>
   <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?><?php the_title(); ?></a></a></li>
<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
	</ul>
	</div>
<!-- ТЕКСТ -->
<p style="text-align: center;"> </p>
<h3 style="text-align: center;"><span style="color: #158a88;">КАКИЕ УСЛУГИ ПРЕДОСТАВЛЯЕТ СЕТЬ КЛИНИК "ДЕНТА-ЭЛЬ"</span></h3>
<p>&nbsp;</p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><a href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;">Многие знают, что стоматология – это раздел медицины, изучающий зубы, их строение, заболевания, лечение и профилактику. В стоматологии, как в науке, выделяют несколько направлений, в зависимости от патологического процесса, а также способов лечения. Давайте кратко рассмотрим каждый вид.</span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060; font-family: georgia, palatino, serif;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/">Терапевтическая стоматология</a></span> занимается проблемами ротовой полости, которые поддаются лечению и коррекции без хирургических вмешательств. Сюда можно отнести три обширные группы заболеваний зубов и ротовой полости:</span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><img class="alignleft" title="Красивая улыбка навсегда" src="http://dentol.ru/wp-content/uploads/2015/11/Terapevticheskoe-napravlenie.png" alt="Забота о пациентах" width="340" height="226" border="0" /></span></div>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;">а) <span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/vosstanovlenie-zubov/">некариозные поражения</a></span> – сюда относятся проблемы, которые связаны с поражением эмали либо приобретенного характера, либо наследственного, повреждение твердых тканей зуба и других проблем, которые возникли не вследствие кариозных проблем.</span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;">б) <span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/terapevticheskaya-stomatologiya/lechenie-kariesa/">кариозные поражения</a></span> – заболевания зуба, вследствие чего образуется полость в твердых тканях зуба, а запущенный кариозный процесс приводит к многочисленным осложнениям. <span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/detskaya-stomatologiya/profilaktika-kariesa/">Кариес</a></span> – это самое распространенное заболевание, которое встречается у людей любого возраста.</span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;">в) заболевания слизистой оболочки ротовой полости – стоматиты, требующие внимательного подхода в лечении. Чаще это заболевание встречается у детей.</span></p>
<div style="float: right; margin: 5px 0px 5px 10px;"><span style="font-size: 16px; color: #616060;"><img class="" title="Красивая улыбка всегда" src="http://dentol.ru/wp-content/uploads/2015/11/Implantologiya-300x181.jpg" alt="Забота о пациентах" width="333" height="201" border="0" /></span></div>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px;"> <span style="font-family: georgia, palatino, serif;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/parodontologiya/">Пародонтология</a></span> – изучает околозубные ткани, занимается лечением и профилактикой таких патологических процессов.</span></span><span style="color: #616060; font-size: 16px;"> </span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/hirurgiya-implantatsiya/">Хирургическая стоматология</a></span> – это направление, где без хирургических вмешательств не обойтись. Чаще это осложнения терапевтических стоматологических заболеваний, но бывают и самостоятельные заболевания – воспаления, которые требуют незамедлительной хирургической операции. Осложнения таких заболеваний могут привести даже к летальному исходу.</span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/hirurgiya-implantatsiya/">Имплантология</a></span> занимает отдельное место в стоматологической нише - это своеобразная наука в науке. Однако, несмотря на то, что данный раздел сразу может показаться сложным для понимания, при первом же с ним ознакомлении четко выявляются сравнительные характеристики использования дорогих и <span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/astra-tech/">передовых видов имплантов (Astra Tech)</a></span> и набирающих обороты на Российском рынке <span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/hirurgiya-implantatsiya/implantyi-cortex/">доступных имплантов.</a></span></span><span style="font-size: 16px; color: #616060;"> </span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/ortopediya-2/">Ортопедическая стоматология</a> </span>занимается вопросами, связанными с восстановлением потерянных зубов, установкой зубных протезов, постимплантологическими манипуляциями и так далее.</span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><img class="" title="Зубная помощь" src="http://dentol.ru/wp-content/uploads/2015/11/Rebenok.jpg" alt="Забота о детях" width="340" height="199" border="0" /></span></div>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/ispravlenie-prikusa/">Ортодонтическая стоматология</a></span> ответственна за правильное расположения зубного ряда во рту, в случае любых отклонений, занимается корректировкой. </span></p>
<div style="float: left; margin: 5px 10px 5px 0px; text-align: justify;"><span style="font-size: 16px; color: #616060;"><a style="color: #616060;" href="http://dentol.ru/o-kompanii/"><img class="alignleft" title="Зуб на абзац" src="http://dentol.ru/wp-content/uploads/2015/08/tooth1.gif" alt="Зубик" width="30" height="30" border="0" /></a></span></div>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;"><span style="color: #0000ff;"><a style="color: #0000ff;" href="http://dentol.ru/uslugi/detskaya-stomatologiya/">Детскую стоматологию</a></span> можно, по праву, назвать отдельной наукой, которая рассматривает все заболевания и процессы, происходящие в ротовой полости ребенка, лечит их и разрабатывает профилактические методы коррекции. К детям необходим особый подход, как и в любой сфере медицины.</span></p>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px;"><strong> </strong></span></p>
<p style="text-align: justify;"><span style="color: #616060; font-size: 16px; font-family: georgia, palatino, serif;"><strong>Дорогие наши, обращаем внимание, что только высококвалифицированный врач-стоматолог может оказать необходимый объем стоматологических услуг.</strong></span></p>
<p style="text-align: justify;"><span style="font-size: 16px; font-family: georgia, palatino, serif;"> </span></p>
<div style="float: right; margin: 5px 10px 5px 0px; text-align: justify;"><a href="http://dentol.ru/o-kompanii/"><img class="alignright" title="Кнопка далее" src="http://dentol.ru/wp-content/uploads/2015/08/new-order-button-blue-01-vosstanovleno1.jpg" alt="Далее" width="140" height="37" border="0" /></a></div>
<div style="float: right; margin: 5px 10px 5px 0px; text-align: justify;"><a href="http://dentol.ru/o-kompanii/"><img class="alignright" title="Стрелка далее" src="http://dentol.ru/wp-content/uploads/2015/08/02.gif" alt="Далее" width="59" height="25" border="0" /></a></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: left;"><span style="font-size: 24px;"><a href="http://dentol.ru/obratnyiy-zvonok/">ЗАКАЗАТЬ ЗВОНОК</a></span></p>
<p>&nbsp;</p>
<!-- ТЕКСТ -->
	<div id="foto">
<?php $galleryСlinics = new WP_Query(array('post_type' => 'galleryСlinics', 'order' => 'ASC')) ?>
<?php if ( $galleryСlinics->have_posts() ) : ?>
     <ul id="flexiselDemo3">
<?php while ( $galleryСlinics->have_posts() ) : $galleryСlinics->the_post(); ?>
<li><a href="<?php echo (get_post_meta($post->ID, 'url', true)); ?>"><?php the_post_thumbnail('250'); ?><span class="acti" style="display:none;"><?php the_title(); ?></span></a></li>
<?php endwhile; ?>
       </ul>
<?php else: ?>
<?php endif; ?>
	</div>
	<div id="lic">
	<?php $args = array( 'post_type' => array('page'),
                    'meta_key' => 'about',
                    'orderby' => 'meta_value_num',
                    'order' => 'ASC',
                    'posts_per_page' => 3 ); ?>
<?php $page_index = new WP_Query($args); ?>

<?php if ( $page_index->have_posts() ) : while ( $page_index->have_posts() ) : $page_index->the_post(); ?>


		<div><a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2><?php the_post_thumbnail('full'); ?></a></div>
		<?php endwhile; ?>
<!-- post navigation -->
<?php endif; ?>
	</div>
<!-- ТЕКСТ -->
		

<p>&nbsp;</p>
<!-- ТЕКСТ -->
 <?php get_footer(); ?>