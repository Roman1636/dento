﻿<?php

function load_style_script(){
	wp_enqueue_script('jquery-google', 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
	wp_enqueue_script('map_yandex_api', '//api-maps.yandex.ru/2.1/?lang=ru_RU');
	wp_enqueue_script('jquery-flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js');
	wp_enqueue_script('yandex_search_map', get_template_directory_uri() . '/js/map.js');
	wp_enqueue_script('jquery-easing', get_template_directory_uri() . '/js/jquery.easing.js');
	wp_enqueue_script('jquery-mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel.js');
	wp_enqueue_script('demo', get_template_directory_uri() . '/js/demo.js');
	wp_enqueue_script('jquery-ui-1.9.2', get_template_directory_uri() . '/js/jquery-ui-1.9.2.custom.js');
	wp_enqueue_script('carousel', get_template_directory_uri() . '/js/carousel.js');
	wp_enqueue_script('flexisel', get_template_directory_uri() . '/js/jquery.flexisel.js');
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
	wp_enqueue_style('style-flexslider', get_template_directory_uri() . '/flexslider.css');
	wp_enqueue_style('style-jquery-ui-1.9.2', get_template_directory_uri() . '/css/jquery-ui-1.9.2.custom.css');
}

add_action('wp_enqueue_scripts', 'load_style_script');

function my_more_options(){
	add_settings_field(
		'phone',
		'Телефон',
		'display_phone', 
		'general' 
	);
	register_setting(
		'general', 
		'my_phone' 
	);
	add_settings_field(
		'mode_work',
		'Режим работы',
		'display_work',
		'general'
	);
	register_setting(
		'general',
		'my_work'
	);
}
add_action('admin_init', 'my_more_options');
function display_phone(){
	echo "<input type='text' class='regular-text' name='my_phone' value='" . esc_attr(get_option('my_phone')) . "'>";
}
function display_work(){
	echo "<input type='text' class='regular-text' name='my_work' value='" . esc_attr(get_option('my_work')) . "'>";
}

register_sidebar(array(
		'name' => 'Социальные иконки в footer',
		'id' => 'icons_header',
		'description' => 'Используйте виджет Текст для добавления HTML-кода иконок',
		'before_widget' => '',
		'after_widget' => ''
	)
);

register_sidebar(array(
		'name' => 'ON-LINE ЗАПИСЬ К ВРАЧУ',
		'id' => 'online_reception',
		'description' => 'Используйте виджет Текст для добавления HTML-кода иконок',
		'before_widget' => '',
		'after_widget' => ''
	)
);

register_sidebar(array(
		'name' => 'Правое меню',
		'id' => 'right_menu',
		'description' => '',
		'before_widget' => '',
		'after_widget' => ''
	)
);

register_sidebar(array(
		'name' => 'Банеры под меню',
		'id' => 'banner_menu',
		'description' => '',
		'before_widget' => '',
		'after_widget' => ''
	)
);

register_nav_menus(array(
	'header_menu' => 'Меню в шапке',
	'footer_menu_center' => 'Меню в подвале О центре',
	'footer_menu_patient' => 'Меню в подвале Для пациентов',
	'footer_menu_usligy' => 'Меню в подвале Услуги'
));

add_action('init', 'slider_index');
function slider_index(){
	register_post_type('slider', array(
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail','custom-fields'),
		'menu_position' => 100,
		'menu_icon' => admin_url() . 'images/media-button-video.gif',
		'labels' => array(
			'name' => 'Слайдер',
			'all_items' => 'Все слайды',
			'add_new' => 'Новый слайд',
			'add_new_item' => 'Добавить слайд'
		)
	));
}

add_action('init', 'gallery_clinics');
function gallery_clinics(){
	register_post_type('galleryСlinics', array(
		'public' => true,
		'supports' => array('title', 'thumbnail','custom-fields'),
		'menu_position' => 100,
		'menu_icon' => admin_url() . 'images/media-button-video.gif',
		'labels' => array(
			'name' => 'Слайдер Клиник',
			'all_items' => 'Все слайды клиник',
			'add_new' => 'Новый слайд клиники',
			'add_new_item' => 'Добавить слайд клиники'
		)
	));
}

add_action('init', 'articles');
function articles(){
	register_post_type('articl', array(
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail','custom-fields'),
		'menu_position' => 20,
		'menu_icon' => admin_url() . 'images/date-button.gif',
		'labels' => array(
			'name' => 'Статьи',
			'all_items' => 'Все статьи',
			'add_new' => 'Новая статья',
			'add_new_item' => 'Добавить статью'
		)
	));
}
//Новости
add_action('init', 'news_list_all');
function news_list_all(){
	register_post_type('all_news', array(
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail','custom-fields'),
		'menu_position' => 20,
		'menu_icon' => admin_url() . 'images/media-button-video.gif',
		'labels' => array(
			'name' => 'Новости',
			'all_items' => 'Все новости',
			'add_new' => 'Новая новость',
			'add_new_item' => 'Добавить новость'
		)
	));
}
//Сми о нас
add_action('init', 'smi_list_all');
function smi_list_all(){
	register_post_type('smi-o-nas', array(
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail','custom-fields'),
		'menu_position' => 20,
		'menu_icon' => admin_url() . 'images/media-button-video.gif',
		'labels' => array(
			'name' => 'СМИ о нас',
			'all_items' => 'Все СМИ',
			'add_new' => 'Новая запись СМИ',
			'add_new_item' => 'Добавить запись СМИ'
		)
	));
}

//Акции
add_action('init', 'acrt_list_all');
function acrt_list_all(){
	register_post_type('aktsii', array(
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail','custom-fields'),
		'menu_position' => 20,
		'menu_icon' => admin_url() . 'images/media-button-video.gif',
		'labels' => array(
			'name' => 'Акции',
			'all_items' => 'Все акции',
			'add_new' => 'Новая акция',
			'add_new_item' => 'Добавить акцию'
		)
	));
}

add_action('init', 'dentol');
function dentol(){
	register_post_type('people', array(
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail','custom-fields'),
		'menu_position' => 21,
		'menu_icon' => admin_url() . 'images/resize.gif',
		'labels' => array(
			'name' => 'Специалисты',
			'all_items' => 'Все специалисты',
			'add_new' => 'Новый Специалист',
			'add_new_item' => 'Добавить Специалиста'
		)
	));
}
add_action('init', 'create_taxonomy');
function create_taxonomy(){
	$labels = array(
		'name'              => 'Клиники',
		'singular_name'     => 'Клиники',
		'search_items'      => 'Поиск клиник',
		'all_items'         => 'Все клиники',
		'parent_item'       => 'Parent Genre',
		'parent_item_colon' => 'Parent Genre:',
		'edit_item'         => 'Редактировать клинику',
		'update_item'       => 'Обновить клинику',
		'add_new_item'      => 'Добавить клинику',
		'new_item_name'     => 'New Genre Name',
		'menu_name'         => 'Клиники',
	); 
	
	$args = array(
		'label'                 => '', 
		'labels'                => $labels,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_tagcloud'         => true,
		'hierarchical'          => false,
		'update_count_callback' => '',
		'rewrite'               => true,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false,
		'_builtin'              => false,
		'show_in_quick_edit'    => null,
	);
	register_taxonomy('taxonomy', array('people'), $args );
}

function new_excerpt_length($length) {
	return 20;
}
add_filter('excerpt_length', 'new_excerpt_length');

function direction($arr){
	$array = Array(
		"terapevticheskaya-stomatologiya"=>"Терапевтическая стоматология",
		"gigiena-i-profilaktika-zubov"=>"Гигиена и профилактика",
		"esteticheskaya-stomatologiya"=>"Эстетическая стоматология",
		"hirurgiya-implantatsiya"=>"Хирургия / имплантация",
		"parodontologiya"=>"Пародонтология",
		"kompyuternaya-tomografiya"=>"Компьютерная томография",
		"stomatologiya-pod-narkozom"=>"Стоматология под наркозом",
		"ortopediya-2"=>"Ортопедия",
		"detskaya-stomatologiya"=>"Детская стоматология",
		"protezirovanie"=>"Протезирование",
		"ispravlenie-prikusa"=>"Исправление прикуса"
	);
	
	foreach($arr as $key){
	$key2 = array_search($key, $array);
	echo "<a class='direction' href='/uslugi/".$key2."'>".$key."</a> 
	| ";
	}
}
function place_work($arr){
	$array = Array(
		"b-r-admirala-ushakova"=>"Бульвар адмирала Ушакова",
		"stomatologiya-na-metro-aeroport-moskva"=>"Стоматология м. Аэропорт",
		"stomatologiya-v-marino-m-bratislavsk"=>"Стоматология м. Братиславская",
		"stomaterapevticheskaya-stomatologiyatologiya-na-m-izmaylovskaya-ul-per"=>"Стоматология м. Измайловская",
		"stomatologiya-u-m-kahovskaya"=>"Стоматология м. Каховская",
		"stomatologiya-v-rayone-stantsii-m-otrad"=>"Стоматология м. Отрадное",
		"stomatologiya-u-m-pl-ilicharimskaya"=>"Стоматология м. Пл. Ильича, Римская",
		"stomatologiya-na-m-polezhaevskaya"=>"Стоматология м. Полежаевская",
		"/stomatologiya-v-rayone-m-rechnoy-vokzal"=>"Стоматология м. Речной вокзал",
		"stomatologiya-v-rayone-m-universitet"=>"Стоматология м. Университет",
		"stomatologiya-u-m-tsvetnoy-bulvar"=>"Стоматология м. Цветной бульвар",
		"stomatologiya-u-m-shodnenskaya"=>"Стоматология м. Сходненская"
	);
	
	foreach($arr as $key){
	$key2 = array_search($key, $array);
	echo " | <a class='' href='/set-klinik/".$key2."'>".$key."</a> 
	| ";
	}
}

add_filter('acf/settings/show_admin', '__return_true');
add_theme_support('post-thumbnails');
add_filter('widget_text', 'do_shortcode');

function remove_menus_bloggood_ru(){
  remove_menu_page( 'wp-responsive-menu' );
}
add_action( 'admin_menu', 'remove_menus_bloggood_ru', 9999);

if( !wp_next_scheduled('reviews_update_action') )
	wp_schedule_event( time(), 'hourly', 'reviews_update_action');

add_action( 'reviews_update_action', 'reviews_update', 10, 3 );
 
function reviews_update() {
	global $wpdb;
	$added_reviews_arr = $wpdb->get_results("SELECT * FROM wp_reviews");
	$reviews_ids_array = array();
	foreach ($added_reviews_arr as $added_review_id){
		$reviews_ids_array[] = $added_review_id->review_in_remote_db;
	}
	if(!empty($reviews_ids_array)){
		$reviews_ids_str = implode(',',$reviews_ids_array);
		$reviews_ids_str = " AND `id` NOT IN (".$reviews_ids_str.")";
	} else {
		$reviews_ids_str = '';
	}
	
	$reviews_db = new mysqli("10.10.66.1", "ryazan", "ryazmobile", "appotziv");
	/* проверка соединения */
	if (mysqli_connect_errno()) {
		printf("Не удалось подключиться: %s\n", mysqli_connect_error());
		exit();
	}
	$reviews_db->set_charset("utf8");
	$reviews_comments = $reviews_db->query("SELECT * FROM `comments` WHERE `type` = 2 AND `visible` = 1 AND `id_clinic` = 2".$reviews_ids_str);
	while ($review = $reviews_comments->fetch_assoc()) {
		$data = array(
			'comment_post_ID'      => 5508,
			'comment_author'       => $review['name'],
			'comment_author_email' => $review['mail'],
			'comment_author_url'   => '',
			'comment_content'      => $review['text'],
			'comment_type'         => '',
			'comment_parent'       => 0,
			'user_id'              => 0,
			'comment_author_IP'    => '78.47.159.175',
			'comment_agent'        => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)',
			'comment_date'         => null,
			'comment_approved'     => 1,
		);

		wp_insert_comment( $data );
		
		$wpdb->insert('wp_reviews',	array( 'review_in_remote_db' => $review['id']));
	}
}

?>