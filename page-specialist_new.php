<?php
/*
Template Name: Специалисты (новая)
*/
?>
<?php get_header('page'); ?>

	<div class="breadcrumb">
		<?php
		if(function_exists('bcn_display'))
		{
			bcn_display();
		}
		?>
	</div>

	<div class="content-main">

		<div class="all_specialist">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			<?php endwhile; ?>
				<!-- post navigation -->
			<?php else: ?>
				<!-- no posts found -->
			<?php endif; ?>


			<?
			$dir_doctor = 1;


			global $wpdb;


			$querystr = "
            SELECT     DISTINCT key1.post_id
            FROM        $wpdb->postmeta key1
            WHERE       key1.meta_key LIKE 'repter_%_ddoctor' and key1.meta_value = $dir_doctor
        ";

			$pageposts = $wpdb->get_results($querystr, OBJECT);
			?>

			<?php if( $pageposts ): ?>
				<h2>Главные врачи</h2>
				<?php foreach( $pageposts as $doctor ): ?>

					<? $doctor->ID = $doctor->post_id?>
					<? $photo = get_field('фото', $doctor->ID);?>

					<div class="page-list_people">
						<img width="150" height="150" src="<?=$photo?>" class="attachment-full wp-post-image" alt="Cпециалист <?=get_the_title( $doctor->ID )?>">
						<h2><?=get_the_title( $doctor->ID )?></h2>
						<?php
						if( have_rows('repter',$doctor->ID) ):
							while ( have_rows('repter',$doctor->ID) ) : the_row();?>
								<p><em><?=the_sub_field('должность');?></p>
								<? break;?>
							<? endwhile;?>
						<? endif;?>
						<a href="<?=get_permalink( $doctor->ID )?>">Подробнее</a>
						<a href="#contact_form_pop" class="fancybox-inline zapis">Записаться на прием</a> <? if( current_user_can('administrator')): ?><a href="<?=get_edit_post_link( $doctor->ID )?>"> Ред. </a><? endif;?>
						<div style="display:none" class="fancybox-hidden">
							<div id="contact_form_pop">
								<h2>ON-LINE ЗАПИСЬ К ВРАЧУ</h2><br>
								[contact-form-7 id="1986" title="Запись на прием"]
							</div>
						</div>
					</div>


				<?php endforeach; ?>
			<?php endif; ?>
			<? wp_reset_postdata();?>

			<?
			$val_int = get_the_id();
			$dir_doctor = 0;
			$querystr = "
            SELECT     DISTINCT key1.post_id
            FROM        $wpdb->postmeta key1
            WHERE       key1.meta_key LIKE 'repter_%_ddoctor' and key1.meta_value = $dir_doctor
        ";

			$pageposts = $wpdb->get_results($querystr, OBJECT);


			?>
			<div class="clearfix"></div>

			<?php if( $pageposts ): ?>
				<h2>Врачи</h2>
				<? $result = array();?>
				<?php foreach( $pageposts as $doctor ): ?>

					<?
					$doctor->ID = $doctor->post_id;
					$proverka = array();

					if( have_rows('repter',$doctor->ID) ):
						while ( have_rows('repter',$doctor->ID) ) : the_row();
							$val = get_sub_field('ddoctor');
							if($val==1):
								$val2 = 1;
							endif;
						endwhile;
					endif;

					if($val2==1):
						continue;
					endif;




					$photo = get_field('фото', $doctor->ID);
					$title = get_the_title( $doctor->ID );
					if( have_rows('repter',$doctor->ID) ):
						while ( have_rows('repter',$doctor->ID) ) : the_row();
							$doljn = '<p><em>'.get_sub_field('должность').'</p>';
							break;
						endwhile;
					endif;

					if( current_user_can('administrator')):
						$adm = '<a href="'.get_edit_post_link( $doctor->ID ).'"> Ред. </a>';
					endif;

					$html = '
                    <div class="page-list_people">
                    <img width="150" height="150" src="'.$photo.'" class="attachment-full wp-post-image" alt="Cпециалист <?=get_the_title( $doctor->ID )?>">
                    <h2>'.$title.'</h2>
                    '.$doljn.'
                    <a href="'.get_permalink( $doctor->ID ).'">Подробнее</a> '.$adm.'

                    <a href="#contact_form_pop" class="fancybox-inline zapis">Записаться на прием</a>
                    <div style="display:none" class="fancybox-hidden">
                        <div id="contact_form_pop">
                            <h2>ON-LINE ЗАПИСЬ К ВРАЧУ</h2><br>
                            [contact-form-7 id="1986" title="Запись на прием"]
                        </div>
                    </div>
                </div>';

					$result[$title] = $html;
					?>
				<?php endforeach; ?>
				<? ksort($result);?>
				<?php foreach( $result as $doctor ): ?>
					<? print $doctor;?>
				<?php endforeach; ?>

			<?php endif; ?>

			<div class="clear"></div>
		</div>
	</div>

<?php get_footer(); ?>